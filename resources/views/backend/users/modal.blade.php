<div class="modal-basic modal fade show" id="modal-basic" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-sm" role="document">
        <div class="modal-content modal-bg-white ">
            <div class="modal-header">
                <h6 class="modal-title">Basic title</h6>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span data-feather="x"></span></button>
            </div>
            <form class="form-horizontal" id="form" action="#">
                <div class="modal-body">
                    <input type="hidden" class="form-control" id="id" disabled>
                    <div class="form-group{{ $errors->has('name') ? ' has-error' : ''}}">
                        {!! Form::label('name', 'Name: ', ['class' => 'control-label']) !!}
                        <div class="with-icon">
                            <span class="la-user lar color-gray"></span>
                            {!! Form::text('name', null, ['class' => 'form-control  ih-medium ip-gray radius-xs b-light', 'id'=>'name','required' => 'required','placeholder'=>'Duran Clayton']) !!}
                        </div>
                        {!! $errors->first('name', '<p class="help-block">:message</p>') !!}
                    </div>
                    <div class="form-group{{ $errors->has('email') ? ' has-error' : ''}}">
                        {!! Form::label('email', 'Email: ', ['class' => 'control-label']) !!}
                        <div class="with-icon">
                            <span class="lar la-envelope color-gray"></span>
                            {!! Form::email('email', null, ['class' => 'form-control  ih-medium ip-gray radius-xs b-light', 'id'=>'email','required' => 'required','placeholder'=>'username@email.com']) !!}
                        </div>
                        {!! $errors->first('email', '<p class="help-block">:message</p>') !!}
                    </div>
                    <div class="form-group{{ $errors->has('password') ? ' has-error' : ''}}">
                        {!! Form::label('password', 'Password: ', ['class' => 'control-label','id'=>'label-password']) !!}
                        <div class="with-icon">
                            <span class="las la-lock color-gray" id="icon-password"></span>
                            <input type="password" class="form-control  ih-medium ip-gray radius-xs b-light" id="password">
                        </div>
                    
                        {!! $errors->first('password', '<p class="help-block">:message</p>') !!}
                    </div>
                    {{-- <div class="form-group{{ $errors->has('licence') ? ' has-error' : ''}}">
                        {!! Form::label('licence', 'Licence: ', ['class' => 'control-label']) !!}
                    
                        <div class="with-icon">
                            <span class="las la-list color-gray" ></span>
                            {!! Form::select('licence_id', $list_licence, null, ['class' => 'form-control','required' => 'required', 'id'=>'licence_id','placeholder' => 'Select Licence']) !!}
                        </div>
                        {!! $errors->first('licence_id', '<p class="help-block">:message</p>') !!}
                    
                    </div> --}}
                    <div class="form-group{{ $errors->has('roles') ? ' has-error' : ''}}">
                        {!! Form::label('role', 'Role: ', ['class' => 'control-label']) !!}
                        {!! Form::select('roles[]', $list_role, isset($user_roles) ? $user_roles : [], ['class' => 'form-control', 'multiple' => true,'id'=>'roles']) !!}
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary btn-sm" id="create" onclick="store()">Save</button>
                    <button type="button" class="btn btn-primary btn-sm" id="update" onclick="updated()">Update</button>
                    <button type="button" class="btn btn-light btn-default btn-squared btn-sm" data-dismiss="modal">Cancel</button>
                </div>
            </form>
        </div>
    </div>
</div>

<script>
    function edit(data){
    
        var role = [];
        Object.values(data.roles).forEach(val => {
            role.push(val.id);
        });
        $('.modal-title').text('Edit Data');
        $('#id').val(data.id);
        $('#name').val(data.name);
        $('#email').val(data.email).attr('readonly', true);
        $('#licence_id').val(data.licence_id);
        $('#roles').val(role);
        $("#password").hide(); 
        $("#label-password").hide();
        $("#icon-password").hide();
        $("#create").hide();
        $("#update").show();
        $('#modal-basic').modal('show');
    }
    
    function create() {
        $('.modal-title').text('Add Data');
        $('#form').trigger("reset");
        $('#email').attr('readonly', false);
        $("#password").show(); 
        $("#label-password").show();
        $("#icon-password").show();
        $("#update").hide();
        $("#create").show();
        $("#modal-basic").modal('show');
    }




function store() {

        var name = $('#name').val();
        var email = $('#email').val();
        var password = $('#password').val();
        var licence_id = $('#licence_id').val();
        var roles = $('#roles').val();
        
        createOverlay("process...");

        $.ajax({
            type : "POST",
            url: "{!! route('users.store') !!}",
            data: {
                '_token' : '{{ csrf_token() }}',
                'name' :name,
                'email' :email,
                'password' :password,
                'licence_id' :licence_id,
                'roles' :roles,
            },
            success: function(data){
                gOverlay.hide();

                if(data["status"] == "success") {            
                    toastr.success(data["message"]);
                    setTimeout(function(){ 
                        window.location = "{{ route('users.index') }}";
                    }, 500);            
                }else {
                    toastr.error(data["message"]);
                }
            },
            error: function(error) {
                alert("Server/network error\r\n" + error);
            }
        });
    }

    function updated(){

        var id = $('#id').val();
        var name = $('#name').val();
        var licence_id = $('#licence_id').val();
        var roles = $('#roles').val();
    
        createOverlay("Proses...");
        $.ajax({
            type : "PUT",
            url: "users/"+id,
            dataType: 'json',
            data: {
                '_token' : '{{ csrf_token() }}',
                'id' : id,
                'name' :name,
                'licence_id' :licence_id,
                'roles' :roles,
            },
            success: function(data){
                gOverlay.hide();
                if(data["status"] == "success") {            
                    toastr.success(data["message"]);
                    setTimeout(function(){ 
                            window.location = "{{ route('users.index') }}";
                    }, 500);              
                }else {
                    toastr.error(data["message"]);
                }
            },
            error: function(error) {
                alert("Server/network error\r\n" + error);
            }
        });
        
    }

</script>