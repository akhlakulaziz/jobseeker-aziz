<div class="modal-basic modal fade show" id="modal-basic-password" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-sm" role="document">
        <div class="modal-content modal-bg-white ">
            <div class="modal-header">
                <h6 class="modal-title">Reset Password</h6>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span data-feather="x"></span></button>
            </div>
            <form class="form-horizontal" id="form" action="#">
                <div class="modal-body">
                    <input type="hidden" class="form-control" id="id_user_reset" disabled>
                  
                    <div class="form-group{{ $errors->has('password_reset') ? ' has-error' : ''}}">
                        {!! Form::label('password_reset', 'New Password: ', ['class' => 'control-label','id'=>'label-password']) !!}
                        <div class="with-icon">
                            <span class="las la-lock color-gray" id="icon-password"></span>
                            <input type="password" class="form-control  ih-medium ip-gray radius-xs b-light" id="password_reset">
                        </div>
                    
                        {!! $errors->first('password_reset', '<p class="help-block">:message</p>') !!}
                    </div>
                    <div class="form-group{{ $errors->has('password_reset_type') ? ' has-error' : ''}}">
                        {!! Form::label('password_reset_type', 'New Password Retype: ', ['class' => 'control-label','id'=>'label-password']) !!}
                        <div class="with-icon">
                            <span class="las la-lock color-gray" id="icon-password"></span>
                            <input type="password" class="form-control  ih-medium ip-gray radius-xs b-light" id="password_reset_type">
                        </div>
                    
                        {!! $errors->first('password_reset_type', '<p class="help-block">:message</p>') !!}
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary btn-sm" id="update" onclick="updated()">Reset Password</button>
                    <button type="button" class="btn btn-light btn-default btn-squared btn-sm" data-dismiss="modal">Cancel</button>
                </div>
            </form>
        </div>
    </div>
</div>

<script>
    function editpassword(data){
        $('#id_user_reset').val(data.id);
        $('#modal-basic-password').modal('show');
    }
    

    function updated(){

        var id = $('#id_user_reset').val();
        var password_reset = $('#password_reset').val();
        var password_reset_type = $('#password_reset_type').val();
    
        createOverlay("Proses...");
        $.ajax({
            type : "POST",
            url: "{!! route('update.password',["id"]) !!}",
            dataType: 'json',
            data: {
                '_token' : '{{ csrf_token() }}',
                'id' : id,
                'password' :password_reset,
                'password_confirmation' :password_reset_type,
            },
            success: function(data){
                gOverlay.hide();
                if(data["status"] == "success") {            
                    toastr.success(data["message"]);
                    setTimeout(function(){ 
                            window.location = "{{ route('users.index') }}";
                    }, 500);              
                }else {
                    toastr.error(data["message"]);
                }
            },
            error: function(error) {
                alert("Server/network error\r\n" + error);
            }
        });
        
    }

</script>