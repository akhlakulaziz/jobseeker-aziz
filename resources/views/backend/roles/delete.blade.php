 <!-- ends: .modal-info-warning -->
 <div class="modal-info-confirmed modal fade show" id="modal-info-confirmed" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-sm modal-info" role="document">
        <div class="modal-content">
            <div class="modal-body">
                <div class="modal-info-body d-flex">
                    <div class="modal-info-icon warning">
                        <span data-feather="info"></span>
                    </div>
                    <div class="modal-info-text">
                        <h6>Do you Want to delete these items?</h6>
                        <p>Some descriptions</p>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
            <input type="hidden" name="id" id="id">
                <button type="button" class="btn btn-light btn-outlined btn-sm" data-dismiss="modal">Cancel</button>
                <button type="button" class="btn btn-danger btn-sm" onclick="destroy()">Delete</button>
            </div>
        </div>
    </div>
</div>
<!-- ends: .modal-info-confirmed -->
<script>

    function remove(id){
        $('#id').val(id);
    }

    function destroy(){
        var id =$('#id').val();
        createOverlay("process...");
        $.ajax({
            type: 'POST',
            dataType: 'json',
            data: {
                '_token' : '{{ csrf_token() }}',
                _method: 'DELETE',
                id: id,
            },
            url: "/admin/roles/"+id,
            success: function(data){
                gOverlay.hide();

                if(data["status"] == "success") {            
                    toastr.success(data["message"]);
                    setTimeout(function(){ 
                        window.location = "{{ route('roles.index') }}";
                    }, 500);            
                }else {
                    toastr.error(data["message"]);
                }
            },
            error: function(error) {
                alert("Server/network error\r\n" + error);
            }
        });
    }


</script>
