@extends('layouts.app')
@section('content')
<div class="contents">
    <div class="container-fluid">
       <div class="row">
          <div class="col-lg-12">
             <div class="contact-breadcrumb">
                <div class="breadcrumb-main add-contact justify-content-sm-between ">
                   <div class=" d-flex flex-wrap justify-content-center breadcrumb-main__wrapper">
                      <div class="d-flex align-items-center add-contact__title justify-content-center mr-sm-25">
                         <h4 class="text-capitalize fw-500 breadcrumb-title">List Roles</h4>
                         <span class="sub-title ml-sm-25 pl-sm-25"></span>
                      </div>
                      <div class="action-btn mt-sm-0 mt-15">
                            @can ('roles-create')
                                <a href="{{ url('/admin/roles/create') }}" class="btn btn-primary btn-sm"><i class="las la-plus fs-16"></i>Add New</a>
                            @endcan
                        </div>
                   </div>
                   <div class="breadcrumb-main__wrapper">
                      <form action="{{ url('/admin/roles') }}" method="GET" class="d-flex align-items-center add-contact__form my-sm-0 my-2">
                         <span data-feather="search"></span>
                         <input class="form-control mr-sm-2 border-0 box-shadow-none" name="search" value="{{ request('search') }}" type="search" id="userName" onkeyup="userFunction()" placeholder="Search by Role Name" aria-label="Search">
                      </form>
                   </div>
                </div>
             </div>
             <!-- ends: contact-breadcrumb -->
          </div>
       </div>
       <div class="row">
          <div class="col-lg-12 mb-30">
             <div class="card">
                <div class="card-header color-dark fw-500">
                 View List
                </div>
                <div class="card-body">
                   <div class="userDatatable global-shadow border-0 bg-white w-100">
                     @include('layouts.partials._message')
                     <div class="row ">
                        <div class="col">
                            <div class="table-responsive">
                                <table class="table">
                                    <thead>
                                    <tr class="userDatatable-header">
                                        <th>
                                           <span class="userDatatable-title">Role</span>
                                        </th>
                                        <th>
                                           <span class="userDatatable-title">Permissions</span>
                                        </th>
                                       <th>
                                          <span class="userDatatable-title">Number of Users</span>
                                       </th>
                                       <th>
                                          <span class="userDatatable-title">Actions</span>
                                       </th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($roles as $item)
                                        <tr>
                                            <td>{{ ucwords($item->name) }}</td>
                                            <td>
                                                @if($item->id == 1)
                                                    All
                                                @else
                                                    @if($item->permissions->count())
                                                        @foreach($item->permissions as $permission)
                                                            {{ ucwords($permission->name) }}
                                                        @endforeach
                                                    @else
                                                        None
                                                    @endif
                                                @endif
                                            </td>
                                            
                                            <td>{{ $item->users->count() }}</td>
                                            
                                            @canany(['roles-edit','roles-delete'])
                                                <td>
                                                    @if($item->id == 1)
                                                         N/A
                                                    @else
                                                        <ul class="mb-0 d-flex flex-wrap">
                                                            @can ('roles-edit')
                                                                <li>
                                                                    <a href="{{ url('/admin/roles/' . $item->id . '/edit') }}" title="Edit Role"><button class="btn btn-icon btn-circle btn-outline-light btn-sm"><span data-feather="edit"></span></button></a>
                                                                </li>
                                                            @endcan
                                                            @can ('roles-delete')
                                                                <li>
                                                
                                                                    <button type="button" onClick="remove({{ $item->id}})" class="btn btn-icon btn-circle btn-outline-light btn-sm" data-toggle="modal" data-target="#modal-info-confirmed" > <span data-feather="trash-2"></span></button>
                                                                </li>
                                                            @endcan
                                                            <li>
                                                            </li>
                                                        </ul>
                                                    @endif
                                                    </div>
                                                </td>
                                            @endcanany
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div><!--col-->
                    </div><!--row-->
                    <div class="row">
                        <div class="col-7">
                            <div class="float-left">
                                {!! $roles->total() !!} roles total
                            </div>
                        </div><!--col-->
            
                        <div class="col-5">
                            <div class="float-right">
                                {!! $roles->appends(['search' => Request::get('search')])->render() !!}
                            </div>
                        </div><!--col-->
                    </div><!--row-->
                   </div>
                </div>
             </div>
          </div>
       </div>
    </div>
 </div>
 @include('backend.roles.delete')
@endsection