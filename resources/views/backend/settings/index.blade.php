@extends('layouts.app')
@section('content')
    <div class="contents">
        <div class="profile-setting ">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="breadcrumb-main">
                            <h4 class="text-capitalize breadcrumb-title">Settings</h4>
                            
                        </div>
                    </div>
                   
                    <div class="col-xl-8 col-lg-8 col-sm-7">
                    
                        <div class="mb-50">
                            <div class="tab-content" id="v-pills-tabContent">
                                <div class="tab-pane fade  show active" id="v-pills-home" role="tabpanel" aria-labelledby="v-pills-home-tab">
                                    <!-- Edit Profile -->
                                    <div class="edit-profile">
                                        <div class="card">
                                            <div class="card-header px-sm-25 px-3">
                                                <div class="edit-profile__title">
                                                    <h6>General</h6>
                                                    <span class="fs-13 color-light fw-400">General settings such as, site title, site description, address and so on.</span>
                                                </div>
                                            </div>
                                            <div class="card-body">
                                                <div class="row justify-content-center">
                                                    <div class="col-xl-6 col-lg-8 col-sm-10">
                                                        <div class="edit-profile__body mx-lg-20">
                                                            <form>
                                                                <div class="form-group mb-20">
                                                                    <label for="name">Name app</label>
                                                                    <input type="text" class="form-control" id="name" value="{{config('app.name')}}">
                                                                </div>
                                                                <div class="form-group mb-20">
                                                                    <label for="url">Url app</label>
                                                                    <input type="text" class="form-control" id="url" value="{{config('app.url')}}">
                                                                </div>
                                                                <div class="form-group mb-20">
                                                                    <label for="url">Description app</label>
                                                                    <textarea class="form-control" name="description" id="description" cols="48" rows="5">{{config('mail.from.app_description')}}</textarea>
                                                                </div>
                                                                <div class="form-group mb-20">
                                                                    <label for="url">Logo app</label>
                                                                    <input type="file" class="form-control" id="logo" >
        
                                                                    <small id="passwordHelpInline2" class="text-light fs-13">
                                                                        Note : Upload logo with <span class="color-dark">black text and transparent background in .png</span> format and <span class="color-dark">294x50(WxH)</span> pixels.
                                                                        <span class="color-dark">Height</span> should be fixed,<span class="color-dark">width</span> according to your  <span class="color-dark">aspect ratio.</span>
                                                                    </small>
                                                                    
                                                                    <div class="col-md-8">
                                                                        <div id="logo_b_image_preview" class="d-inline-block p-3 preview">
                                                                            <img height="50px" src="{{asset('image/'.config('mail.from.app_logo'))}}">
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="form-group mb-20">
                                                                    <label for="favicon">Add Favicon</label>
                                                                    <input type="file" class="form-control" id="favicon" >
        
                                                                    <small id="passwordHelpInline3" class="text-light fs-13">
                                                                        Note : Upload logo with resolution <span class="color-dark">32x32</span> pixels and extension <span class="color-dark">.png</b> or <span class="color-dark">.gif</span> or <span class="color-dark">.ico</span>
                                                                    </small>
                                                                    
                                                                    <div class="col-md-8">
                                                                        <div id="logo_b_image_preview" class="d-inline-block p-3 preview">
                                                                            <img height="50px" src="{{asset('image/'.config('mail.from.app_favicon'))}}">
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                
                                                                <div class="button-group d-flex flex-wrap pt-30 mb-15">
                                                                    <button class="btn btn-primary btn-default btn-squared mr-15 text-capitalize" type="button" onclick="updateGeneral()">update General
                                                                    </button>
                                                                </div>
                                                            </form>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- Edit Profile End -->
                                </div>
                                <div class="tab-pane fade" id="v-pills-profile" role="tabpanel" aria-labelledby="v-pills-profile-tab">
                                    <!-- Edit Profile -->
                                    <div class="edit-profile">
                                        <div class="card">
                                            <div class="card-header  px-sm-25 px-3">
                                                <div class="edit-profile__title">
                                                    <h6>Mail Configuration</h6>
                                                    <span class="fs-13 color-light fw-400">Email SMTP settings, notifications and others related to email.</span>
                                                </div>
                                            </div>
                                            <div class="card-body">
                                                <div class="row justify-content-center">
                                                    <div class="col-xl-6 col-lg-8 col-sm-10">
                                                        <div class="edit-profile__body mx-lg-20">
                                                            <form>
                                                                <div class="form-group mb-20">
                                                                    <label for="mail_name">Mail From Name</label>
                                                                    <input type="text" class="form-control" id="mail_name"  value="{{config('mail.from.name')}}">
                                                                    <small class="font-14 text-muted">This will be display name for your sent email.</small>
                                                                </div>
                                                                <div class="form-group mb-1">
                                                                    <label for="email45">Mail From Address</label>
                                                                    <input type="text" class="form-control" id="mail_address" value="{{config('mail.from.address')}}">
                                                                    <small class="font-14 text-muted">This email will be used for "Contact Form" correspondence.</small>
                                                                </div>
                                                                <div class="form-group mb-1">
                                                                    <label for="email45">Mail Driver</label>
                                                                    <input type="text" class="form-control" id="mail_driver" value="{{config('mail.default')}}">
                                                                    <small class="font-14 text-muted">You can select any driver you want for your Mail setup. Ex. SMTP, Mailgun, Mandrill, SparkPost, Amazon SES etc. Add single driver only..</small>
                                                                </div>
                                                                <div class="form-group mb-1">
                                                                    <label for="email45">Mail HOST</label>
                                                                    <input type="text" class="form-control" id="mail_host" value="{{config('mail.mailers.smtp.host')}}">
                                                                </div>
                                                                <div class="form-group mb-1">
                                                                    <label for="email45">Mail PORT</label>
                                                                    <input type="text" class="form-control" id="mail_port" value="{{config('mail.mailers.smtp.port')}}">
                                                                </div>
                                                                <div class="form-group mb-1">
                                                                    <label for="email45">Mail Username</label>
                                                                    <input type="text" class="form-control" id="mail_username" value="{{config('mail.mailers.smtp.username')}}">
                                                                    <small class="font-14 text-muted">Add your email id you want to configure for sending emails.</small>
                                                                </div>
                                                                <div class="form-group mb-1">
                                                                    <label for="email45">Mail Password</label>
                                                                    <input type="password" class="form-control" id="mail_password" value="{{config('mail.mailers.smtp.password')}}">
                                                                    <small class="font-14 text-muted">Add your email password you want to configure for sending emails.</small>
                                                                </div>
                                                                <div class="form-group mb-1">
                                                                    <label for="email45">email</label>
                                                                    <input type="text" class="form-control" id="mail_encryption"  value="{{config('mail.mailers.smtp.encryption')}}">
                                                                    <small class="font-14 text-muted">Use tls if your site uses HTTP protocol and ssl if you site uses HTTPS protocol.</small>
                                                                </div>
                                                                <div class="form-group mb-1 mt-2">
                                                                    <div class="text-left">
                                                                        <small class="text-muted font-13"><strong>Important Note:</strong></small>
                            
                                                                        <small class="text-muted font-13">IF you are using GMAIL for Mail configuration, make sure you have completed following process before updating: <br>
                                                                            1. Go to <a href="https://myaccount.google.com/security" class="text-blue-600" target="_blank">My Account</a>  from your Google Account you want to configure and Login <br>
                                                                            2. Scroll down to Less secure app access and set it ON <br>
                                                                        </small>
                                                                    </div>
                                                                </div>
                                                                <div class="button-group d-flex flex-wrap pt-30 mb-15">
                                                                    <button class="btn btn-primary btn-default btn-squared mr-15 text-capitalize" type="button" onclick="updateConfiguration()">update Mail Configuration
                                                                    </button>
                                                                </div>
                                                            </form>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- Edit Profile End -->
                                </div>

                            </div>
                        </div>
                    </div>
                    <div class="col-xl-4 col-lg-4 col-sm-5">
                        <!-- Profile Acoount -->
                        <div class="card mb-25">
                            <div class="card-body text-center p-0">
                                
                                <div class="ps-tab p-20 pb-25">
                                    <div class="nav flex-column text-left" id="v-pills-tab" role="tablist" aria-orientation="vertical">
                                        <a class="nav-link active" id="v-pills-home-tab" data-toggle="pill" href="#v-pills-home" role="tab" aria-controls="v-pills-home" aria-selected="true">
                                            <span data-feather="monitor"></span>General</a>
                                        <a class="nav-link" id="v-pills-profile-tab" data-toggle="pill" href="#v-pills-profile" role="tab" aria-controls="v-pills-profile" aria-selected="false">
                                            <span data-feather="mail"></span>Mail Configuration</a>
                    
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- Profile Acoount End -->
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script>
        function updateGeneral(){
            
            var name = $('#name').val();
            var url = $('#url').val();
            var description =$('#description').val();
            var logo = $('#logo').prop('files')[0];
            var favicon = $('#favicon').prop('files')[0];
    
    
            var form_data = new FormData();
            form_data.append('_token', '{{ csrf_token() }}');
            form_data.append('name', name);
            form_data.append('url', url);
            form_data.append('description',description);
            form_data.append('logo', logo);
            form_data.append('favicon', favicon);
            createOverlay("process...");
            $.ajax({
                type : "POST",
                url: "{!! route('settings.store') !!}",
                dataType: 'json',  // what to expect back from the PHP script, if anything
                cache: false,
                contentType: false,
                processData: false,
                data: form_data,                         
                success: function(data){
                    gOverlay.hide();
                    if(data["status"] == "success") {            
                        toastr.success(data["message"]);
                        setTimeout(function(){ 
                            window.location = "{{ route('settings.index') }}";
                        }, 500);            
                    }else {
                        toastr.error(data["message"]);
                    }
                },
                error: function(error) {
                    alert("Server/network error\r\n" + error);
                }
            });
        }
    
        function updateConfiguration(){
            
            var mail_name = $('#mail_name').val();
            var mail_address = $('#mail_address').val();
            var mail_driver =$('#mail_driver').val();
            var mail_host =$('#mail_host').val();
            var mail_port =$('#mail_port').val();
            var mail_username =$('#mail_username').val();
            var mail_password =$('#mail_password').val();
            var mail_encryption =$('#mail_encryption').val();
    
            var form_data = new FormData();
            form_data.append('_token', '{{ csrf_token() }}');
            form_data.append('mail_name', mail_name);
            form_data.append('mail_address', mail_address);
            form_data.append('mail_driver',mail_driver);
            form_data.append('mail_host', mail_host);
            form_data.append('mail_port', mail_port);
            form_data.append('mail_username', mail_username);
            form_data.append('mail_password', mail_password);
            form_data.append('mail_encryption', mail_encryption);
            createOverlay("process...");
            $.ajax({
                type : "POST",
                url: "{!! route('settings.mail') !!}",
                dataType: 'json',  // what to expect back from the PHP script, if anything
                cache: false,
                contentType: false,
                processData: false,
                data: form_data,                         
                success: function(data){
                    gOverlay.hide();
                    if(data["status"] == "success") {            
                        toastr.success(data["message"]);          
                    }else {
                        toastr.error(data["message"]);
                    }
                },
                error: function(error) {
                    alert("Server/network error\r\n" + error);
                }
            });
        }
        
    
       
    </script>
    
@endsection
