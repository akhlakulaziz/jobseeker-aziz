<header class="header-top">
    <nav class="navbar navbar-light">
        <div class="navbar-left">
            @auth()
                <a href="" class="sidebar-toggle">
                    <img class="svg" src="{{ asset('img/svg/bars.svg') }}" alt="img">
                </a>
            @endauth
            <a class="navbar-brand" href="#"><img class="svg dark" src="{{asset('image/'.config('mail.from.app_logo'))}}" alt="">
                <img class="light" src="{{ asset('img/Logo_white.png') }} " alt="">
            </a>
            
    
            @include('layouts.partials._top_menu')
        </div>

        @auth()
            <!-- ends: navbar-left -->
            <div class="navbar-right">
                <ul class="navbar-right__menu">
                    <li style="width: 200px">

                    </li>
                    <!-- ends: nav-message -->
                    <li class="nav-notification" style="width: 200px">
                    </li>
                
                    <li class="nav-author">
                        <div class="dropdown-custom">
                            <a href="javascript:;" class="nav-item-toggle"><img src="{{ asset('img/author-nav.jpg') }}"
                                alt="" class="rounded-circle"></a>
                            <div class="dropdown-wrapper">
                                {{-- <div class="nav-author__info">
                                    <div class="author-img">
                                        <img src="{{ asset('img/author-nav.jpg') }}" alt="" class="rounded-circle">
                                    </div>
                                    <div>
                                        <h6>{{ Auth::user()->name }}</h6>
                                        <span>UI Designer</span>
                                    </div>
                                </div> --}}
                                <div class="nav-author__options">
                                    {{-- <ul>
                                        <li>
                                            <a href="#">
                                                <span data-feather="user"></span> Profile</a>
                                        </li>   
                                    </ul> --}}
                                    <a href="{{ route('logout') }}" class="nav-author__signout" onclick="event.preventDefault();
                                        document.getElementById('logout-form').submit();">
                                        <span data-feather="log-out"></span> Sign Out</a>
                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                                        @csrf
                                    </form>
                                </div>
                            </div>
                            <!-- ends: .dropdown-wrapper -->
                        </div>
                    </li>
                    <!-- ends: .nav-author -->
                </ul>
                <!-- ends: .navbar-right__menu -->
                <div class="navbar-right__mobileAction d-md-none">
                    <a href="#" class="btn-search">
                        <span data-feather="search"></span>
                        <span data-feather="x"></span></a>
                    <a href="#" class="btn-author-action">
                        <span data-feather="more-vertical"></span></a>
                </div>
            </div>
            <!-- ends: .navbar-right -->
        @endauth
    </nav>
</header>
