<div class="col-xxl-3 col-lg-4 col-sm-5">
    <!-- Profile Acoount -->
    <div class="card mb-25">
        <div class="card-body text-center p-0">

            <div class="account-profile border-bottom pt-25 px-25 pb-0 flex-column d-flex align-items-center ">
                <div class="ap-img mb-20 pro_img_wrapper">

                    <label for="file-upload">

                        <img class="ap-img__main rounded-circle wh-120" src="{{ asset('image_profile/'.Auth::user()->basicInformation->image_profile)}}" alt="profile">
                    </label>
                </div>
                <div class="ap-nameAddress pb-3">
                    <h5 class="ap-nameAddress__title">{{Auth::user()->name}}</h5>
                    @if(!empty(Auth::user()->getRoleNames()))
                        @foreach(Auth::user()->getRoleNames() as $key =>$v)
                            <p class="ap-nameAddress__subTitle fs-14 m-0">{{ $v }}</p>
                        @endforeach
                    @endif
                  
                </div>
            </div>
            <div class="ps-tab p-20 pb-25">
                <div class="nav flex-column text-left" id="v-pills-tab" role="tablist" aria-orientation="vertical">
                    <a class="nav-link {{ request()->is('jobseeker/my-profile/basic-information') ? 'active' : ''}}" href="{{route('my-profile.basic-information')}}">
                        <span data-feather="user"></span>Basic information
                    </a>
                    <a class="nav-link {{ request()->is('jobseeker/my-profile/introduction') ? 'active' : ''}}" href="{{route('my-profile.introduction')}}">
                        <span data-feather="clipboard"></span>Introduction
                    </a>
                    <a class="nav-link {{ request()->is('jobseeker/my-profile/qualifications') ? 'active' : ''}}" href="{{route('my-profile.qualifications')}}">
                        <span data-feather="clipboard"></span>Qualifications
                    </a>
                    <a class="nav-link" href="{{route('my-profile.work-history')}}">
                        <span data-feather="coffee"></span>Experience
                    </a>
                    <a class="nav-link {{ request()->is('jobseeker/my-profile/skills') ? 'active' : ''}}" href="{{route('my-profile.skills')}}">
                        <span data-feather="award"></span>Skills
                    </a>
                    <a class="nav-link {{ request()->is('jobseeker/my-profile/languages') ? 'active' : ''}}" href="{{route('my-profile.languages')}}">
                        <span data-feather="command"></span>Languages
                    </a>
                    <a class="nav-link {{ request()->is('jobseeker/my-profile/work-historys') ? 'active' : ''}}" href="{{route('my-profile.work-history')}}">
                        <span data-feather="command"></span>Work History
                    </a>
                    <a class="nav-link {{ request()->is('jobseeker/my-profile/preferences') ? 'active' : ''}}" href="{{route('my-profile.preferences')}}">
                        <span data-feather="command"></span>Preferences
                    </a>
                    <a class="nav-link {{ request()->is('jobseeker/my-profile/resume') ? 'active' : ''}}" href="{{route('my-profile.resume')}}">
                        <span data-feather="command"></span>Resume
                    </a>                    
                </div>
            </div>

        </div>
    </div>
    <!-- Profile Acoount End -->
</div>