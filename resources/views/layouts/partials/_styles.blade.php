
<link href="{{asset('plugins/bootstrap-tagsinput/css/bootstrap-tagsinput.css')}}" rel="stylesheet" />
<link href="{{asset('plugins/bootstrap-select/css/bootstrap-select.min.css')}}" rel="stylesheet" />
<link href="{{asset('plugins/select2/css/select2.min.css')}}" rel="stylesheet" type="text/css" />


<link rel="stylesheet" href="{{ asset('vendor_assets/css/bootstrap/'.(Session::get('layout')=='rtl' ? 'bootstrap-rtl.css' : 'bootstrap.css')) }}">
<link rel="stylesheet" href="{{ asset('vendor_assets/css/daterangepicker.css') }}">
<link rel="stylesheet" href="{{ asset('vendor_assets/css/fontawesome.css') }}">
<link rel="stylesheet" href="{{ asset('vendor_assets/css/footable.standalone.min.css') }}">
<link rel="stylesheet" href="{{ asset('vendor_assets/css/fullcalendar@5.2.0.css') }}">
<link rel="stylesheet" href="{{ asset('vendor_assets/css/jquery-jvectormap-2.0.5.css') }}">
<link rel="stylesheet" href="{{ asset('vendor_assets/css/jquery.mCustomScrollbar.min.css') }}">
<link rel="stylesheet" href="{{ asset('vendor_assets/css/leaflet.css') }}">
<link rel="stylesheet" href="{{ asset('vendor_assets/css/line-awesome.min.css') }}">
<link rel="stylesheet" href="{{ asset('vendor_assets/css/magnific-popup.css') }}">
<link rel="stylesheet" href="{{ asset('vendor_assets/css/MarkerCluster.css') }}">
<link rel="stylesheet" href="{{ asset('vendor_assets/css/MarkerCluster.Default.css') }}">
<link rel="stylesheet" href="{{ asset('vendor_assets/css/select2.min.css') }}">
<link rel="stylesheet" href="{{ asset('vendor_assets/css/slick.css') }}">
<link rel="stylesheet" href="{{ asset('vendor_assets/css/star-rating-svg.css') }}">
<link rel="stylesheet" href="{{ asset('vendor_assets/css/trumbowyg.min.css') }}">
<link rel="stylesheet" href="{{ asset('vendor_assets/css/wickedpicker.min.css') }}">
<link rel="stylesheet" href="{{ asset('css/'.(Session::get('layout')=='rtl' ? 'style-rtl.css' : 'style.css')) }}">
<link rel="stylesheet" type="text/css" href="{{ asset('css/flag-icon.css') }}">
<style>
.bootstrap-tagsinput .tag {
    margin-right: 2px;
    color: #f1f2f6;
    background: #5f63f2;
    border-radius: 4px;
    
    height: 24px;
    margin-bottom: 25px;
    padding: 0px 5px;
}

.bootstrap-tagsinput {
    width: 100% !important;
    background-color: #fff;
    border: 1px solid #ccc;
    box-shadow: inset 0 1px 1px rgb(0 0 0 / 8%);
    display: inline-block;
    padding: 10px 10px;
    color: #555;
    vertical-align: middle;
    border-radius: 4px;
    max-width: 100%;
    line-height: 22px;
    cursor: text;
   

}

.table th, .table td {
    /* padding: 0.75rem; */
    vertical-align: top;
    border-top: 1px solid #f1f2f6;
}

.verticalLine {
  border-left: thick solid #a5a4a4;
  border-right: thick solid #a5a4a4;
}


</style>


<script src="{{asset('js/app.js')}}"></script> 
