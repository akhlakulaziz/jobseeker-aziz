<div class="top-menu">
   <div class="strikingDash-top-menu position-relative">
      <ul>
         <li>
            <a class="#"
               href="{{route('home')}}">Domains</a>
         </li>
         <li class="has-subMenu">
           
            <a href="#" class="#">Billings</a>
            <ul class="subMenu">
             
               <li>
                  <a href="#"
                     class="{{ Route::is('billing.licenses')  ? 'active' : '' }}">
                  <span data-feather="gift" class="nav-icon"></span>
                  <span class="menu-text">License Package</span>
                  </a>
               </li>
               <li>
                  <a href="#"
                     class="{{ Route::is('billing.invoices')  ? 'active' : '' }}">
                  <span data-feather="clipboard" class="nav-icon"></span>
                  <span class="menu-text">Invoices</span>
                  </a>
               </li>
             
            </ul>
         </li>  
      </ul>
   </div>
</div>