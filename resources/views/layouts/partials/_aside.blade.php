<aside class="sidebar">
   <div class="sidebar__menu-group">
       <ul class="sidebar_nav">
           <li class="menu-title">
               <span>Main menu</span>
           </li>
           
           @can('users-list')
            <li class="has-child">
                <a href="#" class="">
                    <span data-feather="grid" class="nav-icon"></span>
                    <span class="menu-text">Master Management</span>
                    <span class="toggle-icon"></span>
                </a>
                <ul>
                    <li>
                        <a class="" href="{{route('categories.index')}}">Categories</a>
                    </li>
                    <li>
                        <a class="" href="{{route('salary-ranges.index')}}">Salary Ranges</a>
                    </li>
                    <li>
                        <a class="" href="{{route('country-codes.index')}}">Country Codes</a>
                    </li>
                    <li>
                        <a class="" href="{{route('sectors.index')}}">Sectors</a>
                    </li>
                    <li>
                        <a class="" href="{{route('locations.index')}}">Locations</a>
                    </li>
                    <li>
                        <a class="" href="{{route('experiences.index')}}">Experiences</a>
                    </li>
                    <li>
                        <a class="" href="{{route('qualifications.index')}}">Qualifications</a>
                    </li>
                    <li>
                        <a class="" href="{{route('nationalities.index')}}">Nationalities</a>
                    </li>
                    <li>
                        <a class="" href="{{route('current-jobs.index')}}">Current Jobs</a>
                    </li>
                    <li>
                        <a class="" href="{{route('levels.index')}}">Levels</a>
                    </li>
                </ul>
            </li>
           @endcan

           @can('users-list')
            <li class="has-child">
                    <a href="#" class="">
                        <span data-feather="users" class="nav-icon"></span>
                        <span class="menu-text">User Management</span>
                        <span class="toggle-icon"></span>
                    </a>
                    <ul>
                        <li class="l_sidebar">
                            <a href="{{route('users.index')}}">Users</a>
                        </li>
                        <li class="l_sidebar">
                            <a href="{{route('roles.index')}}" >Roles</a>
                        </li>
                    </ul>
                </li>
           @endcan
          
           @can('settings-view')
                <li>
                    <a href="{{ route('settings.index') }}" class="{{ Route::is('settings.index') ? 'active' : ''}}">
                    <span data-feather="settings" class="nav-icon"></span>
                    <span class="menu-text">Settings</span>
                    </a>
                </li>
             @endcan
         {{-- jobseekers --}}
         @can('jobseeker-dashboard')
         <li>
            <a href=" {{route('my-profile.basic-information')}}" class="{{ Route::is('my-profile.basic-information') ? 'active' : ''}}">
            <span data-feather="user" class="nav-icon"></span>
            <span class="menu-text">Account</span>
            </a>
         </li>
         @endcan
         @can('employer-dashboard')
         <li>
            {{-- <a href=" {{route('my-profile.index')}}" class="{{ Route::is('my-profile.index') ? 'active' : ''}}">
            <span data-feather="user" class="nav-icon"></span>
            <span class="menu-text">Account</span>
            </a> --}}
         </li>
         @endcan

        <li class="has-child">
            <a href="#" class="">
                <span data-feather="users" class="nav-icon"></span>
                <span class="menu-text">Security</span>
                <span class="toggle-icon"></span>
            </a>
            <ul>
                <li class="l_sidebar">
                    <a href="{{route('security.change-password')}}">Change password</a>
                </li>
                <li class="l_sidebar">
                    <a href="{{route('security.hide-password')}}" >Hide profile (hides the profile from talentpool)</a>
                </li>
            </ul>
        </li>
        
           
        
       </ul>
   </div>
</aside>