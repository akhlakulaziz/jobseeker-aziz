<div class="footer">
    <div class="container">
        <div class="footer-head">
            <div class="row px-2">
                <div class="col">
                    <div class="footer-img"><img src="{{asset('image/'.config('mail.from.app_logo'))}}" alt=""></div>
                </div>
                <div class="col">
                    <div class="footer-socials align-content-center">
                        <ul>
                            <li><a href="#"><i class="im im-facebook"></i></a></li>
                            <li><a href="#"><i class="im im-twitter"></i></a></li>
                            {{-- <li><a href="#"><i class="im im-google-plus"></i></a></li> --}}
                            <li><a href="#"><i class="im im-linkedin"></i></a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <div class="footer-link">
            <div class="container">
                <div class="row">
                    <div class="col">
                        <div class="links">
                            <ul>
                                <h4>For Candidates</h4>
                                <li><a href="#">Browse Jobs</a></li>
                                <li><a href="#">Add Resume</a></li>
                                <li><a href="#">Job Alerts</a></li>
                                <li><a href="#">My Bookmarks</a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="col">
                        <div class="links">
                            <ul>
                                <h4>For Employers</h4>
                                <li><a href="#">Browse Candidates</a></li>
                                <li><a href="#">Post a Job</a></li>
                                <li><a href="#">Post a Task</a></li>
                                <li><a href="#">Plans & Pricing</a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="col">
                        <div class="links">
                            <ul>
                                <h4>Get Help</h4>
                                <li><a href="#">Contact</a></li>
                                <li><a href="#">Privacy Policy</a></li>
                                <li><a href="#">Terms of Use</a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="col">
                        <div class="links">
                            <ul>
                                <h4>Account</h4>
                                <li><a href="#">Log in</a></li>
                                <li><a href="#">My Account</a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="col">
                        <div class="subscribe">
                            <div class="head-subs">
                                <i class="im im-mail"></i>
                                <h4>Sign Up For a Newsletter</h4>
                            </div>
                            <p>Weekly breaking news, analysis and cutting edge advices on job searching.</p>
                            <div class="form-subs">
                                <input type="email" class="form-control" id="exampleFormControlInput1" placeholder="Enter Your Email Address">
                                <button type="submit" class="btn btn-custom"><i class="im im-arrow-right"></i></button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="greeting">
            <hr>
            <p>© 2019 Hireo. All Rights Reserved.</p>
        </div>
    </div>
</div>