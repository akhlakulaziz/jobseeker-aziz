@extends('layouts.frontend')
@section('name_class', 'home')
@section('content')
     <!--Main-hero-Start-->
     <div class="main-hero">
        <div class="parallax-window" data-parallax="scroll" data-image-src="{{asset('img_custome/hero-img.svg')}}">
            <div class="container">
                <div class="hero-title mb-2">
                    <h3>Hire experts or be hired for any job, any time.</h3>
                    <p>Thousands of small businesses use <span>Hireo</span> to turn their ideas into reality.</p>
                </div>
                <div class="searched mb-2">
                    <div class="row">
                        <div class="col">
                            <div class="job-location px-md-1">
                                <div class="badge">
                                    <p>Where?</p>
                                </div>
                                <div class="detail">
                                    <input type="text" name="job-location" id="job-location" placeholder="Online Job">
                                </div>
                            </div>
                            <div class="job-type px-md-1">
                                <div class="badge">
                                    <p>What job you want?</p>
                                </div>
                                <div class="detail">
                                    <input type="text" name="job-type" id="job-type" placeholder="Job title or keywords">
                                </div>
                            </div>
                            <div class="btn-src px-md-1">
                                <button class="btn btn-custom">Search</button>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="info">
                    <div class="row">
                        <div class="col">
                            <div class="jobs px-md-2">
                                <h5>1583</h5>
                                <p>Jobs Posted</p>
                            </div>
                            <!--Divider-->
                            <div class="line"></div>
                            <!--Divider-->
                            <div class="jobs px-md-4">
                                <h5>3543</h5>
                                <p>Task Posted</p>
                            </div>
                            <!--Divider-->
                            <div class="line"></div>
                            <!--Divider-->
                            <div class="jobs px-md-4">
                                <h5>1232</h5>
                                <p>Freelancers</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--Main-hero-End-->

    <!--Featured-Jobs-Start-->
    <div class="featured-jobs">
        <div class="container">
            <div class="fj-hero">
                <h5>Recent Jobs</h5>
                <a href="#">Browse All Jobs <i class="im im-arrow-right"></i></a>
            </div>
            <!--Job 1-->
            <a href="#" class="job-1">
                <div class="company-desc">
                    <div class="images"><img src="{{asset('img_custome/img/company-1.png')}}" alt=""></div>
                    <div class="desc">
                        <div class="title">
                            <h5>Bilingual Event Support Specialist</h5>
                        </div>
                        <ul>
                            <li class="px-md-2">
                                <i class="im im-apartment"></i>
                                <p>Hexagon</p>
                            </li>
                            <li class="px-md-2">
                                <i class="im im-location"></i>
                                <p>San Francisco</p>
                            </li>
                            <li class="px-md-2">
                                <i class="im im-briefcase"></i>
                                <p>Full time</p>
                            </li>
                            <li class="px-md-2">
                                <i class="im im-clock-o"></i>
                                <p>2 days ago</p>
                            </li>
                        </ul>
                    </div>
                    <button class="btn btn-primary">Apply now</button>
                </div>
            </a>
            <!--Job 2-->
            <a href="#" class="job-2">
                <div class="company-desc">
                    <div class="images"><img src="{{asset('img_custome/company-2.png')}}" alt=""></div>
                    <div class="desc">
                        <div class="title">
                            <h5>Barista and Cashier</h5>
                        </div>
                        <ul>
                            <li class="px-md-2">
                                <i class="im im-apartment"></i>
                                <p>Coffee</p>
                            </li>
                            <li class="px-md-2">
                                <i class="im im-location"></i>
                                <p>San Francisco</p>
                            </li>
                            <li class="px-md-2">
                                <i class="im im-briefcase"></i>
                                <p>Full time</p>
                            </li>
                            <li class="px-md-2">
                                <i class="im im-clock-o"></i>
                                <p>2 days ago</p>
                            </li>
                        </ul>
                    </div>
                    <button class="btn btn-primary">Apply now</button>
                </div>
            </a>
            <!--Job 3-->
            <a href="#" class="job-1">
                <div class="company-desc">
                    <div class="images"><img src="{{asset('img_custome/company-3.png')}}" alt=""></div>
                    <div class="desc">
                        <div class="title">
                            <h5>Restaurant General Manajer</h5>
                        </div>
                        <ul>
                            <li class="px-md-2">
                                <i class="im im-apartment"></i>
                                <p>King</p>
                            </li>
                            <li class="px-md-2">
                                <i class="im im-location"></i>
                                <p>San Francisco</p>
                            </li>
                            <li class="px-md-2">
                                <i class="im im-briefcase"></i>
                                <p>Full time</p>
                            </li>
                            <li class="px-md-2">
                                <i class="im im-clock-o"></i>
                                <p>2 days ago</p>
                            </li>
                        </ul>
                    </div>
                    <button class="btn btn-primary">Apply now</button>
                </div>
            </a>
            <!--Job 4-->
            <a href="#" class="job-2">
                <div class="company-desc">
                    <div class="images"><img src="{{asset('img_custome/company-4.png')}}" alt=""></div>
                    <div class="desc">
                        <div class="title">
                            <h5>Restaurant Supervisor</h5>
                        </div>
                        <ul>
                            <li class="px-md-2">
                                <i class="im im-apartment"></i>
                                <p>King</p>
                            </li>
                            <li class="px-md-2">
                                <i class="im im-location"></i>
                                <p>San Francisco</p>
                            </li>
                            <li class="px-md-2">
                                <i class="im im-briefcase"></i>
                                <p>Full time</p>
                            </li>
                            <li class="px-md-2">
                                <i class="im im-clock-o"></i>
                                <p>2 days ago</p>
                            </li>
                        </ul>
                    </div>
                    <button class="btn btn-primary">Apply now</button>
                </div>
            </a>
            <!--Job 5-->
            <a href="#" class="job-1">
                <div class="company-desc">
                    <div class="images"><img src="{{asset('img_custome/company-5.png')}}" alt=""></div>
                    <div class="desc">
                        <div class="title">
                            <h5>International Marketing Coordinator</h5>
                        </div>
                        <ul>
                            <li class="px-md-2">
                                <i class="im im-apartment"></i>
                                <p>Skyist</p>
                            </li>
                            <li class="px-md-2">
                                <i class="im im-location"></i>
                                <p>San Francisco</p>
                            </li>
                            <li class="px-md-2">
                                <i class="im im-briefcase"></i>
                                <p>Full time</p>
                            </li>
                            <li class="px-md-2">
                                <i class="im im-clock-o"></i>
                                <p>2 days ago</p>
                            </li>
                        </ul>
                    </div>
                    <button class="btn btn-primary">Apply now</button>
                </div>
            </a>
        </div>
    </div>
    <!--Featured-Jobs-End-->
@endsection
