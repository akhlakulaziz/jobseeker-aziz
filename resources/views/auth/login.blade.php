@extends('layouts.frontend')
@section('name_class', 'login')
@section('content')
<div class="form-validation">
    <div class="container">
        <div class="form-hero">
            <h4>We're glad to see you again</h4>
            <p>Don't have an account? <a href="{{ route('register') }}">Sign Up!</a></p>
        </div>
        <!--Form-->
        <div class="form-val">
            <div class="card card-default mx-auto">
                <div class="card-header">
                    <h5>Login</h5>
                    <div class="line"></div>
                </div>
                <div class="card-body">
                    <div class="basic-form-wrapper">
                        <form method="POST" action="{{ route('login') }}">
                            @csrf
                            <div class="form-basic">
                                <div class="form-group">
                                    <label>{{ __('E-Mail Address') }}</label>
                                    <input class="form-control form-control-lg" type="text" name="email" placeholder="name@example.com">
                                    @error('email')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                                <div class="form-group">
                                    <label>Password</label>
                                    <div class="input-container position-relative icon-right">
                                        <a href="#" class="input-icon icon-right"><span data-feather="eye-off"></span></a>
                                        <input class="form-control form-control-lg" type="password" name="password" placeholder="Password" required autocomplete="current-password">
                                        @error('password')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>
                                    <div class="form-inline-action d-flex justify-content-between align-items-center">
                                        <div class="checkbox-theme-default custom-checkbox ">
                                            <input class="checkbox" type="checkbox" id="remember" name="remember">
                                            <label for="remember">
                                                <span class="checkbox-text">
                                                    {{ __('Remember Me') }}
                                                </span>
                                            </label>
                                        </div>
                                        @if (Route::has('password.request'))
                                        <a href="{{ route('password.request') }}" class="forgot-pass"> {{ __('Forgot Your Password?') }}</a>
                                        @endif
                                    </div>
                                </div>
                                <div class="form-group mb-1">
                                    <button type="submit" class="btn btn-lg btn-primary btn-submit"> {{ __('Login') }}</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
