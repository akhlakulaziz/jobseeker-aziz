@extends('layouts.frontend')
@section('name_class', 'register')
@section('content')
       <!--Form-Register-Start-->
       <div class="form-validation">
        <div class="container">
            <!--Title-->
            <div class="form-hero">
                <h4>Let's create your account!</h4>
                <p>Already have an account? <a href="{{ route('register') }}">Log in!</a></p>
            </div>
            <!--Form-->
            <div class="form-val">

                <div class="card card-default mx-auto">
                    <div class="card-header">
                        <h5>Register</h5>
                        <div class="line"></div>
                    </div>
                    <div class="card-body">
                        <div class="basic-form-wrapper">
                            <form method="POST" action="{{ route('register') }}">
                                @csrf
                                <!--Switch-Start-->
                                <div class="switch">
                                    <div class="type">
                                        <input type="radio" name="roles" id="freelancer-radio"  value="jobseeker"
                                        checked/>
                                        <label for="freelancer-radio" class="d-flex"><i class="im im-user-male" 24></i><p>Freelancer</p></label>
                                    </div>

                                    <div class="type">
                                        <input type="radio"  name="roles" id="employer-radio" value="employer"  />
                                        <label for="employer-radio" class="d-flex"><i class="im im-briefcase" 24></i><p>Employer</p></label>
                                    </div>
                                </div>
                                <!--Switch-End-->
                                <div class="form-basic">
                                    <div class="form-group">
                                        <label>Name</label>
                                        <input class="form-control form-control-lg @error('name') is-invalid @enderror" type="text" name="name" value="{{ old('name') }}" placeholder="Enter your email" required autocomplete="name" autofocus>
                                    </div>
                                    @error('name')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror

                                    <div class="form-group">
                                        <label>Email Address</label>
                                        <input class="form-control form-control-lg  @error('email') is-invalid @enderror" type="text" name="email" value="{{ old('email') }}" placeholder="Enter your email" required autocomplete="email">
                                        @error('email')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>
                                    <div class="form-group">
                                        <label>{{ __('Password') }}</label>
                                        <div class="input-container position-relative icon-right">
                                            <a href="#" class="input-icon icon-right"><span data-feather="eye-off"></span></a>
                                            <input class="form-control form-control-lg @error('password') is-invalid @enderror" name="password" required autocomplete="new-password" placeholder="Password">
                                            @error('password')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                        </div>
                                        
                                    </div>
                                    <div class="form-group mb-2">
                                        <label>{{ __('Confirm Password') }}</label>
                                        <div class="input-container position-relative icon-right">
                                            <a href="#" class="input-icon icon-right"><span data-feather="eye-off"></span></a>
                                            <input class="form-control form-control-lg" name="password_confirmation" type="password" name="password_confirmation" required autocomplete="new-password" placeholder="Repeat Password">
                                        </div>
                                    </div>
                                    <div class="form-group mb-1">
                                        <button type="submit" class="btn btn-lg btn-primary btn-submit"> {{ __('Register') }}</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--Form-Register-End-->
@endsection
