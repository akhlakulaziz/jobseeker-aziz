<div class="modal-basic modal fade show" id="modal-basic" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-sm" role="document">
        <div class="modal-content modal-bg-white ">
            <div class="modal-header">
                <h6 class="modal-title">Basic title</h6>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span data-feather="x"></span></button>
            </div>
            <form class="form-horizontal" id="form" action="#">
                <div class="modal-body">
                    <input type="hidden" class="form-control" id="id" disabled>
                    <div class="form-group{{ $errors->has('institute') ? ' has-error' : ''}}">
                        {!! Form::label('institute', 'Institute / School: ', ['class' => 'control-label']) !!}
                        <div class="with-icon">
                            <span class="la-user lar color-gray"></span>
                            {!! Form::text('institute', null, ['class' => 'form-control  ih-medium ip-gray radius-xs b-light', 'id'=>'name','required' => 'required','placeholder'=>'Enter institute']) !!}
                        </div>
                        {!! $errors->first('institute', '<p class="help-block">:message</p>') !!}
                    </div>    
                </div>
                <div class="modal-body">
                    <input type="hidden" class="form-control" id="id" disabled>
                    <div class="form-group{{ $errors->has('institute') ? ' has-error' : ''}}">
                        {!! Form::label('institute', 'Institute / School: ', ['class' => 'control-label']) !!}
                        <div class="with-icon">
                            <span class="la-user lar color-gray"></span>
                            {!! Form::text('institute', null, ['class' => 'form-control  ih-medium ip-gray radius-xs b-light', 'id'=>'name','required' => 'required','placeholder'=>'Enter institute']) !!}
                        </div>
                        {!! $errors->first('institute', '<p class="help-block">:message</p>') !!}
                    </div>    
                </div>
                <div class="col-xl-6">
                    <div class="submit-field">
                        <h5>Completed on</h5>
                        <input type="text" class="with-border" name="year" id="year">
                    </div>
                </div>
                <div class="col-xl-6">
                    <div class="submit-field">
                        <h5>Level</h5>
                        
                            <select class="selectpicker with-border" onchange="levels()"data-size="7"  id="level_id" title="Select One">
                                
                            </select>
                       
                            <p>There are no Level options, make it first ...!</p>
                        @endif
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary btn-sm" id="create" onclick="store()">Save</button>
                    <button type="button" class="btn btn-primary btn-sm" id="update" onclick="updated()">Update</button>
                    <button type="button" class="btn btn-light btn-default btn-squared btn-sm" data-dismiss="modal">Cancel</button>
                </div>
            </form>
        </div>
    </div>
</div>

<script>
    function edit(data){
        console.log(data);
        $('.modal-title').text('Edit Data');
        $('#id').val(data.id);
        $('#name').val(data.name);
        $("#create").hide();
        $("#update").show();
        $('#modal-basic').modal('show');
    }
    
    function create() {
        $('.modal-title').text('Add Data');
        $('#form').trigger("reset");
        $("#name").show(); 
        $("#update").hide();
        $("#create").show();
        $("#modal-basic").modal('show');
    }



    function store() {

        var name = $('#name').val();
        createOverlay("process...");

        $.ajax({
            type : "POST",
            url: "{!! route('salary-ranges.store') !!}",
            data: {
                '_token' : '{{ csrf_token() }}',
                'name' :name,
            },
            success: function(data){
                gOverlay.hide();

                if(data["status"] == "success") {            
                    toastr.success(data["message"]);
                    setTimeout(function(){ 
                        window.location = "{{ route('salary-ranges.index') }}";
                    }, 500);            
                }else {
                    toastr.error(data["message"]);
                }
            },
            error: function(error) {
                alert("Server/network error\r\n" + error);
            }
        });
    }

    function updated(){
        var id = $('#id').val();
        var name = $('#name').val();
        createOverlay("Proses...");
        $.ajax({
            type : "PUT",
            url: "{!! route('salary-ranges.update',["id"]) !!}",
            dataType: 'json',
            data: {
                '_token' : '{{ csrf_token() }}',
                'id' : id,
                'name' :name,
            },
            success: function(data){
                gOverlay.hide();
                if(data["status"] == "success") {            
                    toastr.success(data["message"]);
                    setTimeout(function(){ 
                            window.location = "{{ route('salary-ranges.index') }}";
                    }, 500);              
                }else {
                    toastr.error(data["message"]);
                }
            },
            error: function(error) {
                alert("Server/network error\r\n" + error);
            }
        });

    }


</script>