<div id="small-dialog" class="zoom-anim-dialog mfp-hide dialog-with-tabs">

	<!--Tabs -->
	<div class="sign-in-form">

		<ul class="popup-tabs-nav">
			<li><a href="#tab1">Add Qualifications</a></li>
		</ul>

		<div class="popup-tabs-container">

			<!-- Tab -->
			<div class="popup-tab-content" id="tab">

				<form id="terms">
                
                    <div class="col">
                        <div class="row">

                            <div class="col-xl-12">
                                <div class="submit-field">
                                    <h5>Institute / School</h5>
                                    <input type="text" class="with-border" name="institute" id="institute">
                                </div>
                            </div>
                            <div class="col-xl-6">
                                <div class="submit-field">
                                    <h5>Completed on</h5>
                                    <input type="text" class="with-border" name="year" id="year">
                                </div>
                            </div>
                            <div class="col-xl-6">
                                <div class="submit-field">
                                    <h5>Level</h5>
                                    {{-- @if(count($list_levels) > 0) --}}
                                        <select class="selectpicker with-border" onchange="levels()"data-size="7"  id="level_id" title="Select One">
                                            {{-- @foreach($list_levels as $id => $value) --}}
                                                {{-- <option value="{{ $value }}" {{ $value }}>{{ $value->name }}</option> --}}
                                            {{-- @endforeach --}}
                                        </select>
                                    {{-- @else --}}
                                        <p>There are no Level options, make it first ...!</p>
                                    {{-- @endif --}}
                                </div>
                            </div>
                            <div class="col" id="grade" style="display: none;">
                                <h5>Mention your subjects and grade results</h5>
                               <div class="row">
                                    <table  id="dynamic_field">  
                                        <tr>  
                                            <td data-label="Column 1">
                                                <input type="text" name="subject[]" placeholder="Enter Subject" class="with-border subject" />
                                            </td> 
                                            <td data-label="Column 2">
                                                <select class="result with-border" name="result[]" title="More">
                                                    <option value="1">A*</option>
                                                    <option value="2">A</option>
                                                    <option value="3">B</option>
                                                    <option value="4">C</option>
                                                    <option value="5">D</option>
                                                    <option value="6">E</option>
                                                    <option value="7">F</option>
                                                    <option value="8">G</option>
                                                    <option value="9">U</option>
                                                </select>
                                            </td> 
                                            <td data-label="Column 3"><button type="button" name="add" id="add" class="button1 red ripple-effect ico"><i class="icon-material-outline-add"></i></button></td>  
                                        </tr>  
                                    </table> 
                                </div> 
                            </div>

                            <div class="col-xl-12">
                                <div class="submit-field">
                                    <h5>Course Name</h5>
                                    <input type="text" class="with-border" name="course_name" id="course_name">
                                </div>
                            </div>

                        </div>
                    </div>
				</form>

				<!-- Button -->
				<button onclick="storeQualification()" type="button" class="margin-top-15 button full-width button-sliding-icon ripple-effect" type="submit" form="terms">Accept <i class="icon-material-outline-arrow-right-alt"></i></button>

			</div>

		</div>
	</div>
</div>
<!-- Bid Acceptance Popup / End -->

<script>

    function storeQualification() {
            var institute = $('#institute').val();
            var year = $('#year').val();
            var level_id = $('#level_id').val();
            var course_name = $('#course_name').val();
            var count = 1;
            var error = '';
            var subject = new Array();
                $('.subject').each(function() {
                
                    if($(this).val() == '')
                    {
                        error += "<p>Pilih subject dibaris ke-"+count+"</p>";
                        
                    }
                    count = count + 1;
                    subject.push($(this).val());
            });

            var result = new Array();
                $('.result').each(function() {
                
                    if($(this).val() == '')
                    {
                        error += "<p>Masukan result dibaris ke-"+count+"</p>";
                    
                    }
                    count = count + 1;
                    result.push($(this).val());
            });
            createOverlay("Proses...");

            $.ajax({
                type : "POST",
                url: "{!! route('my-profile.work-history-store') !!}",
                data: {
                    '_token' : '{{ csrf_token() }}',
                    'institute' : institute,
                    'year' : year,
                    'level_id' : level_id,
                    'course_name' : course_name,
                    'subject' : subject,
                    'result' : result,
                },
                success: function(data){
                    gOverlay.hide();
                    if(data["status"] == "success") {            
                        toastr.success(data["message"]);
                        setTimeout(function(){ 
                            window.location = "{{ route('my-profile.work_history_post') }}";
                        }, 500); 
                    }else {
                        toastr.error(data["message"]);
                    }
                },
                error: function(error) {
                    alert("Server/network error\r\n" + error);
                }
            });
        }

        function levels(){

            var level_id = $('#level_id').val();
            const obj = JSON.parse(level_id);
            if (obj.status_grade==1) {
                $("#grade").show();
            }else{
                $("#grade").hide();
            }
        }

        $(document).ready(function(){      
        var i=1;  
        $('#add').click(function(){  
            i++;  
            $('#dynamic_field').append('<tr id="row'+i+'" class="dynamic-added"><td><input type="text" name="name[]" placeholder="Enter Subject" class="with-border subject" /></td><td><select  name="result[]" title="Select One" class="result with-border"><option value="1">A*</option><option value="2">A</option><option value="3">B</option><option value="4">C</option><option value="5">D</option><option value="6">E</option><option value="7">F</option><option value="8">G</option><option value="9">U</option></select></td><td><button type="button" name="remove" id="'+i+'" class="button2 red ripple-effect ico btn_remove"><i class="icon-feather-trash-2"></i></button></td></tr>');  
            });  


        $(document).on('click', '.btn_remove', function(){  
            var button_id = $(this).attr("id");   
            $('#row'+button_id+'').remove();  
        });  
        }); 

    
</script>
