@extends('layouts.app')
@section('content')
<div class="contents">
    <div class="profile-setting ">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12">
                    <div class="breadcrumb-main">
                        <h4 class="text-capitalize breadcrumb-title">My profile</h4>
                    </div>
                </div>
                @include('layouts.partials._menu_jobseeker')
                <div class="col-xxl-9 col-lg-8 col-sm-7">
                   
                    <div class="mb-50">
                        <div class="edit-profile mt-0">
                            <div class="card">
                                <div class="card-header px-sm-25 px-3">
                                    <div class="edit-profile__title">
                                        <h6>Tell Employers About your Work History</h6>
                                        <span class="fs-13 color-light fw-400">A good qualification is one key factor to get you hired.</span>
                                    </div>
                                </div>
                                <div class="card-body">
                                    <div class="card">
                                        <div class="card-header color-dark fw-500">
                                            <a data-toggle="modal" data-target="#small-dialog" class="btn btn-success popup-with-zoom-anim">Add Qualifications</a>
                                            {{-- <button type="button" class="btn btn-primary btn-sm"  onClick="create()" data-toggle="modal" data-target="#modal-basic">   <i class="las la-plus fs-16"></i>Add</button> --}}
                                        </div>
                                        <div class="card-body p-0">
                                            <div class="table4  p-25 bg-white mb-30">
                                                <div class="table-responsive">
                                                    <table class="table mb-0">
                                                        <thead>
                                                            <tr class="userDatatable-header">
                                                                <th>
                                                                    <span class="userDatatable-title">No</span>
                                                                </th>
                                                                <th>
                                                                    <span class="userDatatable-title">Category</span>
                                                                </th>
                                                                <th>
                                                                    <span class="userDatatable-title">Resume</span>
                                                                </th>
                                                                <th>
                                                                    <span class="userDatatable-title">Employer</span>
                                                                </th>
                                                                <th>
                                                                    <span class="userDatatable-title">Year From</span>
                                                                </th>
                                                                <th>
                                                                    <span class="userDatatable-title">Year To</span>
                                                                </th>
                                                                <th>
                                                                    <span class="userDatatable-title">Duration</span>
                                                                </th>
                                                                <th>
                                                                    <span class="userDatatable-title">Job Title</span>
                                                                </th>
                                                                <th>
                                                                    <span class="userDatatable-title">Refree Name</span>
                                                                </th>
                                                                <th>
                                                                    <span class="userDatatable-title">Refree Contact</span>
                                                                </th>
                                                                <th>
                                                                    <span class="userDatatable-title">Work Summary</span>
                                                                </th>
                                                                <th>
                                                                    <span class="userDatatable-title">Responsibilities</span>
                                                                </th>
                                                                <th>
                                                                    <span class="userDatatable-title">Action</span>
                                                                </th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                            @foreach ($workHistory as $item)
                                                            <tr>
                                                                <td>
                                                                    <div class="userDatatable-content">
                                                                        {{ $loop->iteration }}
                                                                    </div>
                                                                </td>
                                                                <td>
                                                                    <div class="userDatatable-content">
                                                                        {{$item->category_id}}
                                                                        
                                                                    </div>
                                                                </td>
                                                                <td>
                                                                    <div class="userDatatable-content">
                                                                        {{$item->resume_id}}
                                                                    </div>
                                                                </td>
                                                                <td>
                                                                    <div class="userDatatable-content">
                                                                        {{$item->employer}}
                                                                    </div>
                                                                </td>
                                                                <td>
                                                                    <div class="userDatatable-content">
                                                                        {{$item->year_from}}
                                                                    </div>
                                                                </td>
                                                                <td>
                                                                    <div class="userDatatable-content">
                                                                        {{$item->year_to}}
                                                                    </div>
                                                                </td>
                                                                <td>
                                                                    <div class="userDatatable-content">
                                                                        {{$item->duration}}
                                                                    </div>
                                                                </td>
                                                                <td>
                                                                    <div class="userDatatable-content">
                                                                        {{$item->job_title}}
                                                                    </div>
                                                                </td>
                                                                <td>
                                                                    <div class="userDatatable-content">
                                                                        {{$item->referee_name}}
                                                                    </div>
                                                                </td>
                                                                <td>
                                                                    <div class="userDatatable-content">
                                                                        {{$item->referee_contact}}
                                                                    </div>
                                                                </td>
                                                                <td>
                                                                    <div class="userDatatable-content">
                                                                        {{$item->work_summary}}
                                                                    </div>
                                                                </td>
                                                                <td>
                                                                    <div class="userDatatable-content">
                                                                        {{$item->responsibilities}}
                                                                    </div>
                                                                </td>
                                                                <td>
                                                                    {{-- @can ('users-delete')
                                                                    
                                                                        {!! Form::open([
                                                                            'method'=>'DELETE',
                                                                            'route' => ['survey.destroy', $item->id],
                                                                            'style' => 'display:inline'
                                                                        ]) !!}
                                                                        {!! Form::button('<i class="fi fi-trash"></i>', array(
                                                                                    'type' => 'submit',
                                                                                    
                                                                                    'title' => 'Delete Survey',
                                                                                    'onclick'=>'return confirm("Confirm delete?")'
                                                                            )) !!}
                                                                        {!! Form::close() !!}
                                                                        
                                                                    @endcan --}}
                                                                </td>
                                                            </tr>
                                                            @endforeach
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                            
                </div>
            </div>
        </div>
    </div>
</div>


@include('jobseekers.my-profiles.work-historys.modal')


@endsection