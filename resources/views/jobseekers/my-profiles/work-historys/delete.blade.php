<div id="small-dialog-{{$item->id}}" class="zoom-anim-dialog mfp-hide dialog-with-tabs">
	<div class="sign-in-form">
		<div class="sign-in-form">
			<ul class="popup-tabs-nav">
			</ul>
			<div class="popup-tabs-container">
				<!-- Tab -->
				<div class="popup-tab-content" id="tab2">
					
					<!-- Welcome Text -->
					<div class="welcome-text">
						<h3>Leave a Review</h3>
						<span>Rate <a href="#">Peter Valentín</a> for the project <a href="#">Simple Chrome Extension</a> </span>
					</div>
						
					<!-- Form -->
					<form method="post" id="leave-review-form">
	
						<input type="hidden" name="iddelete" id="iddelete">
						<button class="button red full-width button-sliding-icon ripple-effect" type="submit" form="leave-review-form">Leave a Review <i class="icon-material-outline-arrow-right-alt"></i></button>
					</form>
					
					<!-- Button -->
					
	
				</div>
	
			</div>
		</div>
	</div>
</div>

<script>
	
    function remove(id){
		$('#iddelete').val(id);
    }
    function destroyWorkHistory(){
        var id = $('#iddelete').val();
        createOverlay("process...");
        $.ajax({
            type: 'DELETE',
            dataType: 'json',
            url: "/jobseeker/my-profile/work-history-"+id,
			// url: "{{route('users.destroy',["id"])}}",
            data: {
                '_token' : '{{ csrf_token() }}',
                _method: 'DELETE',
                id: id,
            },
            success: function(data){
                gOverlay.hide();
                if(data["status"] == "success") {            
                    toastr.success(data["message"]);
                    setTimeout(function(){ 
                        window.location = "{{ route('my-profile.work_history_post') }}";
                    }, 500);            
                }else {
                    toastr.error(data["message"]);
                }
            },
            error: function(error) {
                alert("Server/network error\r\n" + error);
            }
        });
    }
</script>