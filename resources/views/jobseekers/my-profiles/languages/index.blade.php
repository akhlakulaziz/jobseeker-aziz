@extends('layouts.app')
@section('content')
<div class="contents">
    <div class="profile-setting ">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12">
                    <div class="breadcrumb-main">
                        <h4 class="text-capitalize breadcrumb-title">My profile</h4>
                    </div>
                </div>
                @include('layouts.partials._menu_jobseeker')
                <div class="col-xxl-9 col-lg-8 col-sm-7">
                   
                    <div class="mb-50">
                        <div class="edit-profile mt-0">
                            <div class="card">
                                <div class="card-header px-sm-25 px-3">
                                    <div class="edit-profile__title">
                                        <h6>Tell employers about your Languages</h6>
                                        <span class="fs-13 color-light fw-400">A good languages is one key factor to get you hired.</span>
                                    </div>
                                </div>
                                <div class="card-body">
                                    <div class="card">
                                        <div class="card-header color-dark fw-500">
                                            <button type="button" class="btn btn-primary btn-sm"  onClick="create()" data-toggle="modal" data-target="#modal-basic">   <i class="las la-plus fs-16"></i>Add</button>
                                        </div>
                                        <div class="card-body p-0">
                                            <div class="table4  p-25 bg-white mb-30">
                                                <div class="table-responsive">
                                                    <table class="table mb-0">
                                                        <thead>
                                                            <tr class="userDatatable-header">
                                                                <th>
                                                                    <span class="userDatatable-title">No</span>
                                                                </th>
                                                                <th>
                                                                    <span class="userDatatable-title">language</span>
                                                                </th>
                                                                <th>
                                                                    <span class="userDatatable-title">Level</span>
                                                                </th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                            @foreach ($languages as $item)
                                                            <tr>
                                                                <td>
                                                                    <div class="userDatatable-content">
                                                                        {{ $loop->iteration }}
                                                                    </div>
                                                                </td>
                                                                <td>
                                                                    <div class="userDatatable-content">
                                                                        {{$item->language_id}}
                                                                    </div>
                                                                </td>
                                                                <td>
                                                                    <div class="userDatatable-content">
                                                                        {{$item->level}}
                                                                    </div>
                                                                </td>
                                                            </tr>
                                                            @endforeach
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                            
                </div>
            </div>
        </div>
    </div>
</div>


@include('jobseekers.my-profiles.qualifications.modal')


@endsection