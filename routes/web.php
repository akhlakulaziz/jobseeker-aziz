<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');


Route::prefix('/admin')->group(function () {
    Route::group(['middleware' => ['auth', 'admin']], function () {
        Route::view('/dashboard','backend.index')->name('admin.dashboard');
        Route::resource('/roles', App\Http\Controllers\Backend\RolesController::class);
        Route::resource('/users', App\Http\Controllers\Backend\UsersController::class);
        Route::resource('/settings', App\Http\Controllers\Backend\ConfigsController::class);
        Route::post('users/{id}/update-password', [App\Http\Controllers\Backend\UsersController::class, 'updatePassword'])->name('update.password');
       
        Route::prefix('/master')->group(function () {
            Route::resource('/categories', App\Http\Controllers\Backend\MasterCategoryController::class);
            Route::resource('/country-codes', App\Http\Controllers\Backend\MasterCountryCodeController::class);
            Route::resource('/current-jobs', App\Http\Controllers\Backend\MasterCurrentJobController::class);
            Route::resource('/experiences', App\Http\Controllers\Backend\MasterExperiencesController::class);
            Route::resource('/languages', App\Http\Controllers\Backend\MasterLanguageController::class);
            Route::resource('/levels', App\Http\Controllers\Backend\MasterLevelController::class);
            Route::resource('/locations', App\Http\Controllers\Backend\MasterLocationController::class);
            Route::resource('/nationalities', App\Http\Controllers\Backend\MasterNationalitiesController::class);
            Route::resource('/qualifications', App\Http\Controllers\Backend\MasterQualificationsController::class);
            Route::resource('/salary-ranges', App\Http\Controllers\Backend\MasterSalaryRangeController::class);
            Route::resource('/sectors', App\Http\Controllers\Backend\MasterSectorController::class);
            Route::resource('/types', App\Http\Controllers\Backend\MasterTypeController::class);
        });

        Route::prefix('/settings')->group(function () {
            Route::post('/mail', [App\Http\Controllers\Backend\ConfigsController::class, 'mail'])->name('settings.mail');
        });
    });
});

Route::prefix('jobseeker')->group(function () {
    Route::group(['middleware' => ['auth', 'jobseeker']], function () {
        Route::view('/dashboard','jobseekers.index')->name('jobseeker.dashboard');

        Route::prefix('my-profile')->group(function () {
            //view
            Route::get('/basic-information', [App\Http\Controllers\ProfileJobSeekerController::class, 'basic_information_view'])->name('my-profile.basic-information');   
            Route::get('/introduction', [App\Http\Controllers\ProfileJobSeekerController::class, 'introduction_view'])->name('my-profile.introduction');   
            Route::get('/qualifications', [App\Http\Controllers\ProfileJobSeekerController::class, 'qualifications_view'])->name('my-profile.qualifications');
            Route::get('/work-history', [App\Http\Controllers\ProfileJobSeekerController::class, 'work_history_view'])->name('my-profile.work-history');
            Route::get('/skills', [App\Http\Controllers\ProfileJobSeekerController::class, 'skills_view'])->name('my-profile.skills');
            Route::get('/languages', [App\Http\Controllers\ProfileJobSeekerController::class, 'languages_view'])->name('my-profile.languages');
            Route::get('/resume', [App\Http\Controllers\ProfileJobSeekerController::class, 'resume_view'])->name('my-profile.resume');
            Route::get('/preferences', [App\Http\Controllers\ProfileJobSeekerController::class, 'preferences_view'])->name('my-profile.preferences');
            
            //
            Route::post('/introduction/update/{id}', [App\Http\Controllers\ProfileJobSeekerController::class, 'introduction_post'])->name('my-profile.introduction-post');
            Route::post('/qualifications-post', [App\Http\Controllers\ProfileJobSeekerController::class, 'qualifications_post'])->name('my-profile.qualifications-post');
            Route::delete('/qualifications-{id}', [App\Http\Controllers\ProfileJobSeekerController::class, 'qualifications_delete'])->name('my-profile.qualifications-delete');
            Route::post('/experience-post', [App\Http\Controllers\ProfileJobSeekerController::class, 'work_history_post'])->name('my-profile.experience-post');
            Route::delete('/experience-{id}', [App\Http\Controllers\ProfileJobSeekerController::class, 'work_history_delete'])->name('my-profile.experience-delete');
            Route::post('/skill-post', [App\Http\Controllers\ProfileJobSeekerController::class, 'skill_post'])->name('my-profile.skill-post');
            Route::post('/skill-update/{id}', [App\Http\Controllers\ProfileJobSeekerController::class, 'skill_update'])->name('my-profile.skill-update');
            Route::delete('/skill-{id}', [App\Http\Controllers\ProfileJobSeekerController::class, 'skill_delete'])->name('my-profile.skill-delete');
            Route::post('/language-post', [App\Http\Controllers\ProfileJobSeekerController::class, 'language_post'])->name('my-profile.language-post');
            Route::delete('/language-{id}', [App\Http\Controllers\ProfileJobSeekerController::class, 'language_delete'])->name('my-profile.language-delete');
            Route::get('/work-history-post', [App\Http\Controllers\ProfileJobSeekerController::class, 'work_history_post'])->name('my-profile.work_history_post');
            Route::post('/work-history-store', [App\Http\Controllers\ProfileJobSeekerController::class, 'work_history_post'])->name('my-profile.work-history-store');
            Route::delete('/work-history-{id}', [App\Http\Controllers\ProfileJobSeekerController::class, 'work_history_delete'])->name('my-profile.work-history-delete');
            //id -- ganti auth
            Route::post('/basic-information/update/{id}', [App\Http\Controllers\ProfileJobSeekerController::class, 'basic_information_update'])->name('update.basic-information');
            Route::post('/preferences/update/{id}', [App\Http\Controllers\ProfileJobSeekerController::class, 'preferences_update'])->name('update.preferences');
        });
    });
});

Route::prefix('employer')->group(function () {
    Route::group(['middleware' => ['auth', 'employer']], function () {
        Route::view('/dashboard','employer.index')->name('employer.dashboard');
    });
});
Route::group(['middleware' => ['auth']], function () {
    Route::prefix('security')->group(function () {
        //view
        Route::get('/change-password', [App\Http\Controllers\ProfileJobSeekerController::class, 'change_password_view'])->name('security.change-password'); 
        Route::get('/hide-password', [App\Http\Controllers\ProfileJobSeekerController::class, 'hide_password_view'])->name('security.hide-password');    
        //post
        Route::post('/post-change-password', [App\Http\Controllers\ProfileJobSeekerController::class, 'change_password_post'])->name('security.change-password-post'); 
        Route::post('/post-hide-password', [App\Http\Controllers\ProfileJobSeekerController::class, 'hide_password_post'])->name('security.hide-password-post');    

        
    });
});