<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use App\Models\JobSeekerBasicInfo;
use App\Models\JobSeekerQualification;
use App\Models\JobSeekerWorkHistory;
use App\Models\QualificationResult;
use App\Models\JobSeekerSkills;
use App\Models\JobSeekerLanguages;
use App\Models\JobSeekerPreferences; 
use App\Models\Resume;
use Auth;
use File;
use Storage;
use Image;
use Hash;
use Validator;


class ProfileJobSeekerController extends Controller
{
    
    public function basic_information_view()
    {
        $basic_information =JobSeekerBasicInfo::where('user_id',Auth::user()->id)->first();
        // dd($basic_information);
        return view('jobseekers.my-profiles.basic-information.index',compact('basic_information'));
        
    }

    public function introduction_view()
    {  
        $basic_information =JobSeekerBasicInfo::where('user_id',Auth::user()->id)->first();
        
        return view('jobseekers.my-profiles.introduction.index',compact('basic_information'));
    }

    public function qualifications_view()
    {
        $qualification = JobSeekerQualification::all();
        return view('jobseekers.my-profiles.qualifications.index',compact('qualification'));
    }

    public function work_history_view()
    {
        $workHistory = JobSeekerWorkHistory::all();
        return view('jobseekers.my-profiles.work-historys.index',compact('workHistory'));
    }

    public function skills_view()
    {
        $basic_information = JobSeekerBasicInfo::where('user_id',Auth::user()->id)->first();
        $skill = JobSeekerSkills::where('job_seeker_basic_info_id', $basic_information->id)->get();
        // dd($skill);
        return view('jobseekers.my-profiles.skills.index',compact('basic_information','skill'));
    }

    public function languages_view()
    {
        $basic_information = JobSeekerBasicInfo::where('user_id',Auth::user()->id)->first();
        $languages = JobSeekerLanguages::where('job_seeker_basic_info_id', $basic_information->id)->get();
        return view('jobseekers.my-profiles.languages.index',compact('basic_information','languages'));
    }

    public function resume_view()
    {
        $basic_information = JobSeekerBasicInfo::where('user_id',Auth::user()->id)->first();
        $resume = Resume::where('job_seeker_basic_info_id', $basic_information->id)->get();
        
        return view('jobseekers.my-profiles.resume.index',compact('basic_information','resume'));

    }

    public function preferences_view()
    {
        $preferences = JobSeekerPreferences::where('user_id',Auth::user()->id)->first();
        // dd($preferences);
        return view('jobseekers.my-profiles.preferences.index',compact('preferences'));
    }

    public function change_password_view()
    {
        return view('jobseekers.security.change-password');
    }

    public function hide_password_view()
    {
        return view('home');
    }

    //post
    public function change_password_post(Request $request){

        Validator::extend('password', function ($attribute, $value, $parameters, $validator) {
            return Hash::check($value, \Auth::user()->password);
        });

        $rules =array(
            'current_password'      => 'required|password',
            'password'              => 'required|min:8|confirmed',
            'password_confirmation' => 'required',
        );
        $validator=Validator::make($request->all(),$rules);
        
        if($validator->fails())
        {
            $messages=$validator->messages();
            $errors=$messages->all();
            return response()->json(["status"=>"error","message"=>$errors[0]], 200);
        }

        
        $user = User::find(Auth::id());

        $user->password = bcrypt(request('password'));
        $user->save();
        return response()->json(["status"=>"success","message"=>'Password has been updated!'], 200);
    
    }

    public function basic_information_update(Request $request, $id)
    {
        // dd($request->all());
        $basic_information = JobSeekerBasicInfo::where('id', $id)->first();

        if ($request->hasFile('image_profile')) 
        {
            $rules =array(
                'image_profile' => 'nullable|file|image|max:2048',
            );
            $validator=Validator::make($request->all(),$rules);
            
            if($validator->fails())
            {
                $messages=$validator->messages();
                $errors=$messages->all();
                return response()->json(["status"=>"error","message"=>$errors[0]], 200);
            }

            $image = $request->file('image_profile');
            
            //name file
            $imagename = $image->getClientOriginalName();
            $a = explode(".", $imagename);
            $fileExt = strtolower(end($a));  
                $namaFile = substr(md5(date("YmdHis")),0,10).".".$fileExt;
            
            //penyimpanan
            $destination_path= public_path().'/image_profile/';
                
            // simpan ke folder
            $request->file('image_profile')->move($destination_path,$namaFile);

            // simpan nama ke database
            $basic_information->image_profile = $namaFile;
        }

        $basic_information->name = $request->name;
        $basic_information->country_code_id = $request->country_code_id;
        $basic_information->phone = $request->phone;
        $basic_information->gender = $request->gender;
        $basic_information->nationality_id = $request->nationality_id;
        $basic_information->dob = $request->dob;
        $basic_information->employment_status = $request->employment_status;
        $basic_information->qualification_id = $request->qualification_id;
        $basic_information->experience_id = $request->experience_id;
        $basic_information->experience_id = $request->experience_id;
        $basic_information->location_id = $request->location_id;
        $basic_information->update();

        return response()->json(["status"=>"success","message"=>'Profile Updated!'], 200);
    }


    public function introduction_post(Request $request,$id){
        $basic_information = Resume::where('id', $id)->first();
        $basic_information->introduction = $request->introduction;
        $basic_information->update();

        return response()->json(["status"=>"success","message"=>'Introduction Updated!'], 200);
    }

    public function qualifications_post(Request $request)
    {
        // dd($request->all());
        // dd($request->subject['0']);
        // dd(preg_split('/(@|,|:|")/', $request->level_id));
        $levelId = preg_split('/(@|,|:|")/', $request->level_id);
        // dd($levelId['3']);

        $qualification = new JobSeekerQualification();
        $qualification->level_id = $levelId['3'];
        $qualification->resume_id = $request->resume_id;
        $qualification->institute = $request->institute;
        $qualification->year = $request->year;
        $qualification->course_name = $request->course_name;
        $qualification->save();

        if($request->subject['0'] != NULL)
        {
            foreach($request->subject as $key=> $item){
                $qualification_result = QualificationResult::create([
                    'js_qualifications_id'=>$qualification->id,
                    'subject'=>$item,
                    'result'=>$request->result[$key],
                ]);
            }
        }

        return response()->json(["status"=>"success","message"=>'Qualification Updated!'], 200);
    }
    public function qualifications_delete($id)
    {
        JobSeekerQualification::destroy($id);

        return redirect()->back();
        // return response()->json(["status"=>"success","message"=>'Qualification deleted!'], 200);
    }

    public function work_history_post(Request $request)
    {
        // dd($request->all());

        $workHistory = new JobSeekerWorkHistory();
        $workHistory->category_id = $request->category_id;
        $workHistory->resume_id = $request->resume_id;
        $workHistory->employer = $request->employer;
        $workHistory->year_from = $request->year_from;
        $workHistory->year_to = $request->year_to;
        $workHistory->duration = $request->duration;
        $workHistory->job_title = $request->job_title;
        $workHistory->referee_name = $request->referee_name;
        $workHistory->referee_contact = $request->referee_contact;
        $workHistory->work_summary = $request->work_summary;
        $workHistory->responsibilities = $request->responsibilities;
        $workHistory->save();

        return response()->json(["status"=>"success","message"=>'Work History Created!'], 200);

    }

    public function work_history_delete($id)
    {
        JobSeekerWorkHistory::destroy($id);

        return redirect()->back();
        // return response()->json(["status"=>"success","message"=>'Qualification deleted!'], 200);
    }

    public function skill_post(Request $request)
    {
        // dd($request->all());

        $rules =array(
            'title'      => 'required',
        );
        $validator=Validator::make($request->all(),$rules);
        
        if($validator->fails())
        {
            $messages=$validator->messages();
            $errors=$messages->all();
            return response()->json(["status"=>"error","message"=>$errors[0]], 200);
        }
        
        $basic_information = JobSeekerBasicInfo::where('user_id', Auth::user()->id)->first();

        $skill = new JobSeekerSkills();
        $skill->job_seeker_basic_info_id = $basic_information->id;
        $skill->title = $request->title;
        $skill->level = $request->level;
        $skill->save();

        return response()->json(["status"=>"success","message"=>'Skill added!'], 200);

    }

    public function skill_update(Request $request, $id)
    {
        dd($request->all());

        $rules =array(
            'title'      => 'required',
        );
        $validator=Validator::make($request->all(),$rules);
        
        if($validator->fails())
        {
            $messages=$validator->messages();
            $errors=$messages->all();
            return response()->json(["status"=>"error","message"=>$errors[0]], 200);
        }
        
        // $basic_information = JobSeekerBasicInfo::where('user_id', Auth::user()->id)->first();

        $skill = JobSeekerSkills::where('id', $id)->first();
        // $skill->job_seeker_basic_info_id = $basic_information->id;
        $skill->title = $request->title;
        $skill->level = $request->level;
        $skill->update();

        return response()->json(["status"=>"success","message"=>'Skill Updated!'], 200);

    }

    public function skill_delete($id)
    {
        JobSeekerSkills::destroy($id);

        return redirect()->back();
        // return response()->json(["status"=>"success","message"=>'Qualification deleted!'], 200);
    }

    public function language_post(Request $request)
    {
        // dd($request->all());
        
        $basic_information = JobSeekerBasicInfo::where('user_id', Auth::user()->id)->first();

        $languages = new JobSeekerLanguages();
        $languages->job_seeker_basic_info_id = $basic_information->id;
        $languages->language_id = $request->language;
        $languages->level = $request->level;
        $languages->save();

        return response()->json(["status"=>"success","message"=>'Language added!'], 200);

    }

    public function language_delete($id)
    {
        JobSeekerLanguages::destroy($id);

        return redirect()->back();
        // return response()->json(["status"=>"success","message"=>'Qualification deleted!'], 200);
    }

    public function preferences_update(Request $request, $id)
    {
        // dd($request->all());
        $preferences = JobSeekerPreferences::where('id', $id)->first();

        $preferences->location_id = $request->location_id;
        $preferences->sector_id = $request->sector_id;
        $preferences->category_id = $request->category_id;
        $preferences->type_id = $request->type_id;
        $preferences->update();

        return response()->json(["status"=>"success","message"=>'Preference Updated!'], 200);
    }
}
