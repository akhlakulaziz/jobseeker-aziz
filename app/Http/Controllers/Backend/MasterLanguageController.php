<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\MasterLanguage;
use Illuminate\Support\Facades\Validator;
class MasterLanguageController extends Controller
{
    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $table_name = "Language Table";
        $table_information = "Master Language Table";
        $perPage = 25;

        if (!empty($keyword)) {
            $collection = MasterLanguage::where('name', 'LIKE', "%$keyword%")->paginate($perPage);
        } else {
            $collection = MasterLanguage::paginate($perPage);
        }

        return view('backend.master-language.index', compact('collection','table_name','table_information'));
    }
    
    public function store(Request $request)
    {
        $rules =array(
            'name' => 'required|unique:master_languages,name',
        );
        $validator=Validator::make($request->all(),$rules);
        if($validator->fails())
        {
            $messages=$validator->messages();
            $errors=$messages->all();
            return response()->json(["status"=>"error","message"=>$errors[0]], 200);
        }

        MasterLanguage::create([
            'name' => $request->name,
        ]);
        return response()->json(["status"=>"success","message"=>'Language added!'], 200);
    
    }
    public function update(Request $request)
    {
        $rules =array(
            'name' => 'required|unique:master_languages,name,' .$request->name,
        );
        
        $validator=Validator::make($request->all(),$rules);
        
        if($validator->fails())
        {
            $messages=$validator->messages();
            $errors=$messages->all();
            return response()->json(["status"=>"error","message"=>$errors[0]], 200);
        }
        $requestData = $request->all();
        $collection = MasterLanguage::findOrFail($requestData['id']);
        $collection->update($requestData);
        return response()->json(["status"=>"success","message"=>'Language updated!'], 200);
    
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return void
     */
    public function destroy($id)
    {
        MasterLanguage::destroy($id);
        return response()->json(["status"=>"success","message"=>'Language deleted!'], 200);
    }
    
}
