<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\MasterType;
use Illuminate\Support\Facades\Validator;

class MasterTypeController extends Controller
{
    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $table_name = "Type Table";
        $table_information = "Master Type Table";
        $perPage = 25;

        if (!empty($keyword)) {
            $collection = MasterType::where('name', 'LIKE', "%$keyword%")->paginate($perPage);
        } else {
            $collection = MasterType::paginate($perPage);
        }

        return view('backend.master-type.index', compact('collection','table_name','table_information'));
    }

    
    public function store(Request $request)
    {
        $rules =array(
            'name' => 'required|unique:master_types,name',
        );
        $validator=Validator::make($request->all(),$rules);
        if($validator->fails())
        {
            $messages=$validator->messages();
            $errors=$messages->all();
            return response()->json(["status"=>"error","message"=>$errors[0]], 200);
        }

        MasterType::create([
            'name' => $request->name,
        ]);
        return response()->json(["status"=>"success","message"=>'Category added!'], 200);
    
    }
    public function update(Request $request)
    {
        $rules =array(
            'name' => 'required|unique:master_types,name,' .$request->name,
        );
        
        $validator=Validator::make($request->all(),$rules);
        
        if($validator->fails())
        {
            $messages=$validator->messages();
            $errors=$messages->all();
            return response()->json(["status"=>"error","message"=>$errors[0]], 200);
        }
        $requestData = $request->all();
        $collection = MasterType::findOrFail($requestData['id']);
        $collection->update($requestData);
        return response()->json(["status"=>"success","message"=>'Type updated!'], 200);
    
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return void
     */
    public function destroy($id)
    {
        MasterType::destroy($id);
        return response()->json(["status"=>"success","message"=>'Type deleted!'], 200);
    }
    
}
