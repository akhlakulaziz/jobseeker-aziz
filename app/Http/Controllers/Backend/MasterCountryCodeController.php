<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\MasterCountryCode;
use Illuminate\Support\Facades\Validator;

class MasterCountryCodeController extends Controller
{
    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $table_name = "Country Code Table";
        $table_information = "Master Country Code Table";
        $perPage = 25;

        if (!empty($keyword)) {
            $collection = MasterCountryCode::where('name', 'LIKE', "%$keyword%")->paginate($perPage);
        } else {
            $collection = MasterCountryCode::paginate($perPage);
        }

        return view('backend.master-country-code.index', compact('collection','table_name','table_information'));
    }

    public function store(Request $request)
    {
        $rules =array(
            'name' => 'required|unique:master_country_codes,name',
        );
        $validator=Validator::make($request->all(),$rules);
        if($validator->fails())
        {
            $messages=$validator->messages();
            $errors=$messages->all();
            return response()->json(["status"=>"error","message"=>$errors[0]], 200);
        }

        MasterCountryCode::create([
            'name' => $request->name,
        ]);
        return response()->json(["status"=>"success","message"=>'Country Code added!'], 200);
    
    }
    public function update(Request $request)
    {
        $rules =array(
            'name' => 'required|unique:master_country_codes,name,' .$request->name,
        );
        
        $validator=Validator::make($request->all(),$rules);
        
        if($validator->fails())
        {
            $messages=$validator->messages();
            $errors=$messages->all();
            return response()->json(["status"=>"error","message"=>$errors[0]], 200);
        }
        $requestData = $request->all();
        $collection = MasterCountryCode::findOrFail($requestData['id']);
        $collection->update($requestData);
        return response()->json(["status"=>"success","message"=>'Country Code updated!'], 200);
    
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return void
     */
    public function destroy($id)
    {
        MasterCountryCode::destroy($id);
        return response()->json(["status"=>"success","message"=>'Country Code deleted!'], 200);
    }
}
