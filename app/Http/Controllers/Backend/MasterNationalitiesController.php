<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\MasterNationalities;
use Illuminate\Support\Facades\Validator;

class MasterNationalitiesController extends Controller
{
    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $table_name = "Nationalities Table";
        $table_information = "Master Nationalities Table";
        $perPage = 25;

        if (!empty($keyword)) {
            $collection = MasterNationalities::where('name', 'LIKE', "%$keyword%")
                ->paginate($perPage);
        } else {
            $collection = MasterNationalities::paginate($perPage);
        }

        return view('backend.master-nationalities.index', compact('collection','table_name','table_information'));
    }

    
    public function store(Request $request)
    {
        $rules =array(
            'name' => 'required|unique:master_nationalities,name',
        );
        $validator=Validator::make($request->all(),$rules);
        if($validator->fails())
        {
            $messages=$validator->messages();
            $errors=$messages->all();
            return response()->json(["status"=>"error","message"=>$errors[0]], 200);
        }

        MasterNationalities::create([
            'name' => $request->name,
        ]);
        return response()->json(["status"=>"success","message"=>'Nationality added!'], 200);
    
    }
    public function update(Request $request)
    {
        $rules =array(
            'name' => 'required|unique:master_nationalities,name,' .$request->name,
        );
        
        $validator=Validator::make($request->all(),$rules);
        
        if($validator->fails())
        {
            $messages=$validator->messages();
            $errors=$messages->all();
            return response()->json(["status"=>"error","message"=>$errors[0]], 200);
        }
        $requestData = $request->all();
        $collection = MasterNationalities::findOrFail($requestData['id']);
        $collection->update($requestData);
        return response()->json(["status"=>"success","message"=>'Nationality updated!'], 200);
    
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return void
     */
    public function destroy($id)
    {
        MasterNationalities::destroy($id);
        return response()->json(["status"=>"success","message"=>'Nationality deleted!'], 200);
    }
    
}
