<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\MasterQualification;
use Illuminate\Support\Facades\Validator;

class MasterQualificationsController extends Controller
{
    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $table_name = "Qualification Table";
        $table_information = "Master Qualification Table";
        $perPage = 25;

        if (!empty($keyword)) {
            $collection = MasterQualification::where('name', 'LIKE', "%$keyword%")
                ->paginate($perPage);
        } else {
            $collection = MasterQualification::paginate($perPage);
        }

        return view('backend.master-qualification.index', compact('collection','table_name','table_information'));
    }
    
    public function store(Request $request)
    {
        $rules =array(
            'name' => 'required|unique:master_qualifications,name',
        );
        $validator=Validator::make($request->all(),$rules);
        if($validator->fails())
        {
            $messages=$validator->messages();
            $errors=$messages->all();
            return response()->json(["status"=>"error","message"=>$errors[0]], 200);
        }

        MasterQualification::create([
            'name' => $request->name,
        ]);
        return response()->json(["status"=>"success","message"=>'Qualification added!'], 200);
    
    }
    public function update(Request $request)
    {
        $rules =array(
            'name' => 'required|unique:master_qualifications,name,' .$request->name,
        );
        
        $validator=Validator::make($request->all(),$rules);
        
        if($validator->fails())
        {
            $messages=$validator->messages();
            $errors=$messages->all();
            return response()->json(["status"=>"error","message"=>$errors[0]], 200);
        }
        $requestData = $request->all();
        $collection = MasterQualification::findOrFail($requestData['id']);
        $collection->update($requestData);
        return response()->json(["status"=>"success","message"=>'Qualification updated!'], 200);
    
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return void
     */
    public function destroy($id)
    {
        MasterQualification::destroy($id);
        return response()->json(["status"=>"success","message"=>'Qualification deleted!'], 200);
    }
    
}
