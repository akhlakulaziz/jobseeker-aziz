<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\MasterExperience;
use Illuminate\Support\Facades\Validator;
class MasterExperiencesController extends Controller
{
    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $table_name = "Experience Table";
        $table_information = "Master Experience Table";
        $perPage = 25;

        if (!empty($keyword)) {
            $collection = MasterExperience::where('name', 'LIKE', "%$keyword%")
                ->paginate($perPage);
        } else {
            $collection = MasterExperience::paginate($perPage);
        }

        return view('backend.master-experience.index', compact('collection','table_name','table_information'));
    }
    
    public function store(Request $request)
    {
        $rules =array(
            'name' => 'required|unique:master_experiences,name',
        );
        $validator=Validator::make($request->all(),$rules);
        if($validator->fails())
        {
            $messages=$validator->messages();
            $errors=$messages->all();
            return response()->json(["status"=>"error","message"=>$errors[0]], 200);
        }

        MasterExperience::create([
            'name' => $request->name,
        ]);
        return response()->json(["status"=>"success","message"=>'Experience added!'], 200);
    
    }
    public function update(Request $request)
    {
        $rules =array(
            'name' => 'required|unique:master_experiences,name,' .$request->name,
        );
        
        $validator=Validator::make($request->all(),$rules);
        
        if($validator->fails())
        {
            $messages=$validator->messages();
            $errors=$messages->all();
            return response()->json(["status"=>"error","message"=>$errors[0]], 200);
        }
        $requestData = $request->all();
        $collection = MasterExperience::findOrFail($requestData['id']);
        $collection->update($requestData);
        return response()->json(["status"=>"success","message"=>'Experience updated!'], 200);
    
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return void
     */
    public function destroy($id)
    {
        MasterExperience::destroy($id);
        return response()->json(["status"=>"success","message"=>'Experience deleted!'], 200);
    }
    
}
