<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Models\Configs;
class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        foreach (Configs::all() as $setting) {
            \Illuminate\Support\Facades\Config::set($setting->key, $setting->value);
        }
    }
}
