<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use App\Models\MasterNationalities;
use App\Models\MasterQualification;
use App\Models\MasterExperience;
use App\Models\MasterLanguage;
use App\Models\MasterCurrentJob; 
use App\Models\MasterCountryCode;
use App\Models\MasterLocation; 
use App\Models\MasterLevel;
use App\Models\MasterCategory;
use App\Models\MasterType;
use App\Models\MasterSector;

use App\Models\License;


class FormServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        view()->composer(['backend.roles.form','backend.users.modal'], function($view) {
            $view->with('permissions', Permission::pluck('name', 'id'));
            $view->with('list_role', Role::pluck('name', 'id'));
        });

        view()->composer(['jobseekers.my-profiles.basic-information.index'], function($view) {
            $view->with('list_nationalities', MasterNationalities::pluck('name', 'id'));
            $view->with('list_qualification', MasterQualification::pluck('name', 'id'));
            $view->with('list_experience', MasterExperience::pluck('name', 'id'));
            $view->with('list_country_code', MasterCountryCode::pluck('name', 'id'));
            $view->with('list_location', MasterLocation::pluck('name', 'id'));
        });

        view()->composer(['jobseekers.my-profiles.qualifications.modal'], function($view) {
            $view->with('list_levels', MasterLevel::get());
        });

        view()->composer(['jobseekers.my-profiles.work-historys.modal'], function($view) {
            $view->with('list_categories', MasterCategory::get());
        });

        view()->composer(['jobseekers.my-profiles.languages.modal'], function($view) {
            $view->with('list_language', MasterLanguage::get());
        });

        view()->composer(['jobseekers.my-profiles.preferences'], function($view) {
            $view->with('list_location', MasterLocation::pluck('name', 'id'));
            $view->with('list_sector', MasterSector::pluck('name', 'id'));
            $view->with('list_categories', MasterCategory::pluck('name', 'id'));
            $view->with('list_type', MasterType::pluck('name', 'id'));
        });

        view()->composer(['companies.basic-information','companies.sub-company.create','companies.sub-company.edit'], function($view) {
            $view->with('list_sector', MasterSector::pluck('name', 'id'));
            $view->with('list_country_code', MasterCountryCode::pluck('name', 'id'));
        });
    }
}
