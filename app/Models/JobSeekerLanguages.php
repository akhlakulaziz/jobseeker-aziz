<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\MasterLanguage;

class JobSeekerLanguages extends Model
{
    use HasFactory;
    protected $guarded = [];

    public function languages()
    {
        $this->belongsTo(MasterLanguage::class);
    }
}
