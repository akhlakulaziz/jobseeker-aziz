<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\MasterQualification;
class MasterQualificationSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        MasterQualification::create([
            'name' => '2',
        ]);
    }
}
