<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\MasterLevel;
class MasterLevelsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $levels = [
            ['id' => 1, 'name' => 'Grade 7 Standard', 'status_grade'=>1],
            ['id' => 2, 'name' => 'O Level','status_grade'=>1],
            ['id' => 3, 'name' => 'Certificate 1','status_grade'=>0],
            ['id' => 4, 'name' => 'Certificate 2','status_grade'=>0],
            ['id' => 5, 'name' => 'Certificate 3','status_grade'=>0],
            ['id' => 6, 'name' => 'A Level','status_grade'=>1],
            ['id' => 7, 'name' => 'Diploma','status_grade'=>0],
            ['id' => 8, 'name' => 'Advanced Diploma','status_grade'=>0],
            ['id' => 9, 'name' => 'Associates Degree','status_grade'=>0],
            ['id' => 10, 'name' => "Bachelor's Degree",'status_grade'=>0],
            ['id' => 11, 'name' => "Master's Degree",'status_grade'=>0],
            ['id' => 12, 'name' => "PHD",'status_grade'=>0],
        ];
  
        foreach ($levels as $item) {
            MasterLevel::create($item);
        }

    }
}
