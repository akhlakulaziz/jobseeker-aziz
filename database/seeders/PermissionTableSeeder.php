<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

class PermissionTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $admin = Role::create(['name' => 'admin']);
        $employer = Role::create(['name' => 'employer']);
        $jobseeker = Role::create(['name' => 'jobseeker']);

        $permissions = [

            ['id' => 1, 'name' => 'users-list',],
            ['id' => 2, 'name' => 'users-create',],
            ['id' => 3, 'name' => 'users-edit',],
            ['id' => 4, 'name' => 'users-delete',],
            ['id' => 5, 'name' => 'users-view',],

            ['id' => 6, 'name' => 'roles-list',],
            ['id' => 7, 'name' => 'roles-create',],
            ['id' => 8, 'name' => 'roles-edit',],
            ['id' => 9, 'name' => 'roles-delete',],
            ['id' => 10, 'name' => 'roles-view',],

            ['id' => 11, 'name' => 'admin-dashboard',],
            ['id' => 12, 'name' => 'employer-dashboard',],
            ['id' => 13, 'name' => 'jobseeker-dashboard',],
            ['id' => 14, 'name' => 'settings-view',],

        ];

        foreach ($permissions as $item) {
            Permission::create($item);
        }
        
        $employer_permissions = [12];
        $jobseeker_permissions = [13];
        
        $admin->syncPermissions(Permission::all());
        $employer->syncPermissions($employer_permissions);
        $jobseeker->syncPermissions($jobseeker_permissions);

    }
}
