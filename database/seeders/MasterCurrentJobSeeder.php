<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\MasterCurrentJob;
class MasterCurrentJobSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        MasterCurrentJob::create([
            'name' => 'job',
        ]);
    }
}
