<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class ConfigTableSeeder extends Seeder
{
  
    public function run()
    {
        $data = [
            'app.name' =>'laravel',
            'app.url' => 'http://project-tracking-livewire-3.test',
            'mail.from.name' => 'wide',
            'mail.from.address' => 'yanafriyokoboja@gmail.com',
            'mail.default' => 'smtp',
            'mail.mailers.smtp.host' => 'smtp.gmail.com',
            'mail.mailers.smtp.port' => '587',
            'mail.mailers.smtp.username' => 'yanafriyokoboja@gmail.com',
            'mail.mailers.smtp.password' => '1qazxsw2@yan12345',
            'mail.mailers.smtp.encryption' => 'tls',
            'mail.from.app_logo' =>'logo.png',
            'mail.from.app_favicon' =>'favicon.ico',
            'mail.from.app_description' =>'Build successful SEO campaigns from the ground up. Hundreds of businesses have already set up the very foundation of workflow management and SEO reporting with us',
      
        ];

        foreach ($data as $key => $value) {
            $config = \App\Models\Configs::firstOrCreate(['key' => $key]);
            $config->value = $value;
            $config->save();
        }
    }
}
