<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\JobSeekerWorkHistory;
class JobSeekerWorkHistoriesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        JobSeekerWorkHistory::create([
            'category_id' => '1',
            'resume_id' => '1',
            'employer' => 'PT Usaha',
            'year_from' => '2021-07-22',
            'year_to' => '2021-07-23',
            'duration' => '1 tahun',
            'job_title' => 'Sales',
            'referee_name' => 'Adit',
            'referee_contact' => '123456789102',
            'work_summary' => 'menjual Produk Perusahaan',
            'responsibilities' => 'melakukan penjualan produk',
        ]);
    }
}
