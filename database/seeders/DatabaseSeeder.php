<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(ConfigTableSeeder::class);
        $this->call(MasterCategorySeeder::class);
        $this->call(MasterCountryCodeSeeder::class);
        $this->call(MasterCurrentJobSeeder::class);
        $this->call(MasterExperienceSeeder::class);
        $this->call(MasterLanguagesSeeder::class);
        $this->call(MasterLevelsSeeder::class);
        $this->call(MasterLocationSeeder::class);
        $this->call(MasterNationalitiesSeeder::class);
        $this->call(MasterQualificationSeeder::class);
        $this->call(MasterSalaryRangeSeeder::class);
        $this->call(MasterSectorSeeder::class);
        $this->call(MasterTypeSeeder::class);
        $this->call(PermissionTableSeeder::class);
        $this->call(UserSeeder::class);
        $this->call(JobSeekerWorkHistoriesSeeder::class);
        $this->call(JobSeekerLanguagesSeeder::class);
        $this->call(JobSeekerQualificationsSeeder::class);
        
    }
}
