<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\JobSeekerLanguages;
class JobSeekerLanguagesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        JobSeekerLanguages::create([
            'job_seeker_basic_info_id' => '1',
            'language_id' => '1',
            'level' => 'Medium',
        ]);
    }
}
