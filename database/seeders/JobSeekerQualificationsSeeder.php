<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\JobSeekerQualification;
class JobSeekerQualificationsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        JobSeekerQualification::create([
            'level_id' => '1',
            'resume_id' => '1',
            'institute' => 'Oracle',
            'year' => '2021',
            'course_name' => 'Network Fundamental',
        ]);
    }
}
