<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateQualificationResultsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('qualification_results', function (Blueprint $table) {
            $table->id();
            $table->foreignId('js_qualifications_id')->nullable()->comment('job_seeker_basic_info_id');
            $table->string('subject')->nullable();
            $table->string('result')->nullable()->comment('1 - A*, 2 - B, 3-C, 4-D, 5-E, 6-F, 7-G,8-U');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('qualification_results');
    }
}
