<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateJobSeekerWorkHistoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('job_seeker_work_histories', function (Blueprint $table) {
            $table->id();
            $table->foreignId('category_id')->nullable();
            $table->foreignId('resume_id')->nullable();
            $table->string('employer')->nullable();
            $table->date('year_from')->nullable();
            $table->date('year_to')->nullable();
            $table->string('duration')->nullable();
            $table->string('job_title')->nullable();
            $table->string('referee_name')->nullable();
            $table->string('referee_contact')->nullable();
            $table->text('work_summary')->nullable();
            $table->string('responsibilities')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('job_seeker_work_histories');
    }
}
