<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateResumesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('resumes', function (Blueprint $table) {
            $table->id();
            $table->foreignId('job_seeker_basic_info_id')->nullable();
            $table->text('introduction')->nullable();
            $table->integer('layout')->default('0');
            $table->tinyInteger('noAchievements')->default('0');
            $table->tinyInteger('noSkills')->default('0'); 
            $table->tinyInteger('noHobbies')->default('0');
            $table->tinyInteger('requested_vetting')->default('0');
            $table->tinyInteger('is_vetted')->default('0');
            $table->string('headline')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('resumes');
    }
}
